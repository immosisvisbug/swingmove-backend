'use-strict';

const {
  BAD_REQUEST
} = require('http-status-codes');

const config = require('../../../config');
const { createHttpError } = require('../../shared/error');
const moment = require('moment-timezone');

const { Video, Course, Played, Sequelize } = require('../../database');
const {addUrlToVideo} = require('../../shared/video');

async function createCourse({ user, body }, res) {
  if (user.membership != config.membership.ADMIN) {
    await createHttpError(BAD_REQUEST, 'Only Admin Users can access this.');
  }

  var { title } = body;

  if (!title) {
    await createHttpError(BAD_REQUEST, 'Please enter the title of the Course .');
  };

  let course = await Course.make({title: title});

  res.json({ data: { course: course } });
}

// Admin/Paid USer Function
async function getCourse({ user, params }, res) {
  if (user.membership === config.membership.FREE) {
    await createHttpError(BAD_REQUEST, 'Only Paid/Admin Users can access this.');
  }
  let courseId = params.course;

  let course = await Course.findOne({
    where: {
      id: courseId
    }
  });

  if (!course) {
    await createHttpError(BAD_REQUEST, 'A course with this id does not exist');
  };

  let videos = await Video.findAll({
    where: {
      courseId: course.id
    }
  });

  let cleanedVideos = await addUrlToVideo(videos);

  course.dataValues.videos = cleanedVideos;

  res.json({ data: { course: course } });
}

// Admin/Paid USer Function
async function getCourses({ user, params }, res) {
  if (user.membership == config.membership.FREE) {
    await createHttpError(BAD_REQUEST, 'Only Paid/Admin Users can access this.');
  }

  let courses = await Course.findAll({order: [
    ['updated_at', 'ASC']
  ]});

  for (let index in courses) {
    let videos = await Video.findAll({
      where: {
        courseId: courses[index].dataValues.id
      }
    });

    let playedVideos = await Played.findAll({
      where: {
        userId: user.id
      },
      order: [
        ['lastPlayed', 'DESC'] // Sorts by COLUMN_NAME_EXAMPLE in ascending order
      ]
    });

    let latestPlayed = playedVideos[0];

    let cleanedVideos = await addUrlToVideo(videos);

    courses[index].dataValues.videos = cleanedVideos;
    courses[index].dataValues.latestPlayed = latestPlayed ? latestPlayed.videoWatched : undefined;
  }

  res.json({ data: { courses: courses } });
}

// Admin/Paid USer Function
async function deleteCourse({ user, params }, res) {
  if (user.membership != config.membership.ADMIN) {
    await createHttpError(BAD_REQUEST, 'Only Admin Users can access this.');
  }
  let courseId = params.course;

  let course = await Course.findOne({
    where: {
      id: courseId
    }
  });

  if (!course) {
    await createHttpError(BAD_REQUEST, 'A course with this id does not exist');
  };

  let deleted = await course.destroy();
  res.json({ data: { deleted: deleted} });
}

module.exports = {
  createCourse,
  getCourse,
  getCourses,
  deleteCourse
};
