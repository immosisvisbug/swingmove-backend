'use-strict';

const ct = require('./course.controller');
const router = require('express-promise-router')();

router
  .post('/', ct.createCourse)
  .get('/:course', ct.getCourse)
  .get('/', ct.getCourses)
  .delete('/:course', ct.deleteCourse);

module.exports = { router };
