'use-strict';

const config = require('../../../config');
const values = require('lodash/values');

module.exports = function (sequelize, DataTypes) {
  const { STRING, DATE, UUID, UUIDV4, INTEGER } = DataTypes;

  const Course = sequelize.define('course', {
    id: {
      type: UUID,
      primaryKey: true,
      defaultValue: UUIDV4
    },
    title: {
      type: STRING,
      field: 'title'
    },

  }, {
    underscored: true,
    timestamps: true,
    freezeTableName: true
  });

    // Class methods

  Course.make = async function (data) {
    return Course.create(data);
  };

  return Course;
};
