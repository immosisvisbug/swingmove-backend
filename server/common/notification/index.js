'use-strict';

const ct = require('./notification.controller');
const router = require('express-promise-router')();

router
  .post('/', ct.createNotification)
  .patch('/:id', ct.editNotification)
  .delete('/:id', ct.deleteNotification)
  .get('/', ct.listNotifications);

module.exports = { router };
