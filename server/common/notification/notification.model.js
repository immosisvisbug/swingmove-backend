'use-strict';

const config = require('../../../config');
const values = require('lodash/values');

module.exports = function (sequelize, DataTypes) {
  const { BOOLEAN, DATE, UUID, UUIDV4, ENUM, INTEGER} = DataTypes;

  const Notification = sequelize.define('notification', {
    id: {
      type: UUID,
      primaryKey: true,
      defaultValue: UUIDV4
    },
    userId: {
      type: UUID,
      allowNull: false,
      field: 'user_id'
    },
    type: {
      type: ENUM(values(config.notificationType)),
      allowNull: false,
      field: 'type'
    },
    startTime: {
      type: DATE,
      allowNull: true,
      field: 'start_time'
    },
    enabled: {
      field: 'enabled',
      allowNull: false,
      type: BOOLEAN,
      defaultValue: false
    },
    endTime: {
      type: DATE,
      allowNull: true,
      field: 'end_time'
    },
    interval: {
      type: INTEGER,
      allowNull: true
    },
    dailyTime: {
      type: DATE,
      allowNull: true,
      field: 'daily_time'
    }
  }, {
    underscored: true,
    timestamps: true,
    freezeTableName: true
  });

    // Class methods

  Notification.make = async function (data) {
    return Notification.create(data);
  };

  return Notification;
};
