'use-strict';

const {
  BAD_REQUEST
} = require('http-status-codes');

const config = require('../../../config');
const { createHttpError } = require('../../shared/error');
const request = require('request-promise');
const { send } = require('../../shared/mail');
const pick = require('lodash/pick');
const toUpper = require('lodash/toUpper');

const { Notification, Sequelize } = require('../../database');

async function createNotification({ body, user }, res) {
  var { type } = body;

  if (!type) {
    await createHttpError(BAD_REQUEST, 'Please enter type of Notification.');
  };

  var data = pick(body, ['startTime', 'endTime', 'dailyTime', 'interval', 'enabled']);
  const cleanedType = config.notificationType[toUpper(type)];

  data['userId'] = user.id;
  data['type'] = cleanedType;

  let notification = await Notification.make(data);

  res.json({ data: { notification: notification } });
}

async function editNotification({ body, user, params }, res) {
  let id = params.id;

  if (!id) {
    await createHttpError(BAD_REQUEST, 'Please provide id of the notification to edit');
  };

  let notification = await Notification.findOne({
    where: {
      id: id
    }
  });

  if (!notification) {
    await createHttpError(BAD_REQUEST, 'A notification with this id does not exist');
  };

  const data = pick(body, ['startTime', 'endTime', 'interval', 'dailyTime', 'enabled']);

  await notification.update(data);

  res.json({ data: { notification: notification } });
}

async function deleteNotification({ params }, res) {
  let id = params.id;

  if (!id) {
    await createHttpError(BAD_REQUEST, 'Please provide id of the notification to delete');
  };

  let notification = await Notification.findOne({
    where: {
      id: id
    }
  });

  if (!notification) {
    await createHttpError(BAD_REQUEST, 'A notification with this id does not exist');
  };

  await notification.destroy();
  notification.dataValues.enabled = false;

  res.json({ data: { notification: notification } });
}

async function listNotifications({ user }, res) {
  const notifications = await Notification.findAll({
    where: {
      userId: user.id
    }
  });

  res.json({ data: { notifications: notifications } });
}

module.exports = {
  createNotification,
  editNotification,
  listNotifications,
  deleteNotification
};
