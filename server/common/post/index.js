'use-strict';

const ct = require('./post.controller');
const router = require('express-promise-router')();

router
  .post('/', ct.createPost)
  .get('/posts', ct.viewBlogPosts)
  .get('/newsletters', ct.viewNewsletters)
  .get('/:id', ct.viewBlogPost)
  .delete('/:id', ct.deleteBlogPost)
  .patch('/:id', ct.editBlogPost);

module.exports = { router };
