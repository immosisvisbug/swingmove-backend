'use-strict';

const {
  BAD_REQUEST
} = require('http-status-codes');

const config = require('../../../config');
const { createHttpError } = require('../../shared/error');
const request = require('request-promise');
const pick = require('lodash/pick');

const { Post, Sequelize } = require('../../database');

// Admin User Function
async function createPost({ body, user }, res) {
  if (user.membership != config.membership.ADMIN) {
    await createHttpError(BAD_REQUEST, 'Only Admin Users can access this.');
  }

  const { title, postBody, isNewsletter } = body;
  if (!title || !postBody) {
    await createHttpError(BAD_REQUEST, 'Please enter title and Body.');
  };

  body.author = user.id;
  await Post.make(body);

  res.json({ data: { msg: 'Post Created Sucessfully'} });
}

// Admin User Function

// Edit a plog post based on the id
async function editBlogPost({ user, body, params}, res) {
  if (user.membership != config.membership.ADMIN) {
    await createHttpError(BAD_REQUEST, 'Only Admin Users can access this.');
  }

  let id = params.id;

  if (!id) {
    await createHttpError(BAD_REQUEST, 'Please provide id of the blog post to edit');
  };

  let post = await Post.findOne({
    where: {
      id: id
    }
  });

  const data = pick(body, ['title', 'postBody', 'isNewsletter']);

  await post.update(data);

  res.json({ data: { post: post } });
}

// Display a plog post based on the id
async function viewBlogPost({ user, params }, res) {
  let id = params.id;

  if (!id) {
    await createHttpError(BAD_REQUEST, 'Please provide id of the blog post to view');
  };

  let post = await Post.findAll({
    where: {
      isNewsletter: false,
      id: id
    }
  });

  res.json({ data: { post: post } });
}

// Display a list of all blog posts that are not newsletters
async function viewBlogPosts({ user }, res) {
  let posts = await Post.findAll({
    where: {
      isNewsletter: false
    }
  });

  res.json({ data: { posts: posts } });
}

// Display a list of all newsletters
async function viewNewsletters({ user }, res) {
  let newsletters = await Post.findAll({
    where: {
      isNewsletter: true
    }
  });

  res.json({ data: { newsletters: newsletters } });
}

// Admin User Function
// Delete a blog post based on the id
async function deleteBlogPost({ user, params }, res) {
  if (user.membership != config.membership.ADMIN) {
    await createHttpError(BAD_REQUEST, 'Only Admin Users can access this.');
  }
  let id = params.id;

  if (!id) {
    await createHttpError(BAD_REQUEST, 'Please provide id of the blog post to delete');
  };
  let post = await Post.findOne({
    where: {
      id: id
    }
  });

  await post.destroy();
  res.json({ data: { messages: 'Post Deleted Sucessfully' } });
}

module.exports = {
  createPost,
  editBlogPost,
  viewBlogPost,
  viewBlogPosts,
  deleteBlogPost,
  viewNewsletters
};
