'use-strict';

const config = require('../../../config');
const values = require('lodash/values');

module.exports = function (sequelize, DataTypes) {
  const { STRING, DATE, UUID, UUIDV4, BOOLEAN, TEXT } = DataTypes;

  const Post = sequelize.define('post', {
    id: {
      type: UUID,
      primaryKey: true,
      defaultValue: UUIDV4
    },
    title: {
      type: STRING,
      allowNull: false,
      field: 'title'
    },
    postBody: {
      type: TEXT,
      allowNull: false,
      field: 'post_body'
    },
    author: {
      type: UUID,
      allowNull: false,
      field: 'author'
    },
    isNewsletter: {
      type: BOOLEAN,
      validate: { notEmpty: true },
      allowNull: false,
      defaultValue: false,
      field: 'is_newsletter'
    },
  }, {
    underscored: true,
    timestamps: true,
    freezeTableName: true
  });

    // Class methods

  Post.make = async function (data) {
    return Post.create(data);
  };

  return Post;
};
