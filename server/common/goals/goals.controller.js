'use-strict';

const {
  BAD_REQUEST
} = require('http-status-codes');

const config = require('../../../config');
const { createHttpError } = require('../../shared/error');
const moment = require('moment-timezone');

const { Goals, Progression, Sequelize } = require('../../database');

// TODO Find out if I need the complete video information or just the id
// TODO Make sure it takes into reference user's local timezone for checking

async function listDailyCompleted({ user }, res) {
  // Get user's timezone
  let timeZone = user.timeZone;
  let currentDate = moment().tz(timeZone);

  const completedWorkouts = await Progression.findAll({
    where: {
      userId: user.id
    },
    order: [
      ['created_at', 'DESC'] // Sorts by COLUMN_NAME_EXAMPLE in ascending order
    ]
  });

  let dailyWorkouts = await completedWorkouts.filter(completed => {
    // Check
    let completedDate = moment(completed.dataValues.created_at).tz(timeZone);
    var isToday = currentDate.isSame(completedDate, 'day');

    if (isToday) {
      return completed;
    }
  });

  res.json({ data: { completed: dailyWorkouts } });
}

async function changeDaily({ user, body }, res) {
  var { dailyGoal } = body;

  if (!dailyGoal) {
    await createHttpError(BAD_REQUEST, 'Please give a daily goal');
  };

  let goal = await Goals.findOne({
    where: {
      userId: user.id
    }
  });

  goal = await goal.update({dailyWorkoutGoal: dailyGoal});

  res.json({ data: { goal: goal } });
}

// TODO Find out if I need the video instead of just the video id
async function getGoals({ user }, res) {
  let goal = await Goals.findOne({
    where: {
      userId: user.id
    }
  });

  if (!goal) {
    goal = await Goals.make({userId: user.id, streak: 0});
  }

  res.json({ data: { goal: goal } });
}

module.exports = {
  listDailyCompleted,
  getGoals,
  changeDaily
};
