'use-strict';

const ct = require('./goals.controller');
const router = require('express-promise-router')();

router
  .get('/dailycompleted', ct.listDailyCompleted)
  .get('/', ct.getGoals)
  .patch('/', ct.changeDaily);


module.exports = { router };
