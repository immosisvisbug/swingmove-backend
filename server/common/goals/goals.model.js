'use-strict';

const config = require('../../../config');
const values = require('lodash/values');

module.exports = function (sequelize, DataTypes) {
  const { STRING, DATE, UUID, UUIDV4, INTEGER } = DataTypes;

  const Goals = sequelize.define('goals', {
    userId: {
      type: UUID,
      allowNull: false,
      field: 'user_id',
      primaryKey: true
    },
    // Holds the longest time that the user has followed their goals
    streak: {
      type: INTEGER,
      allowNull: false,
      defaultValue: 0,
      field: 'streak'
    },
    // Holds the longest time that the user has done a workout in a row
    dayStreak: {
      type: INTEGER,
      allowNull: false,
      defaultValue: 0,
      field: 'day_streak'
    },
    latestWorkoutWatched: {
      type: UUID,
      allowNull: true,
      field: 'latest_workout'
    },
    lastWorkoutGoal: {
      type: DATE,
      allowNull: true,
      field: 'latest_goal'
    },
    dailyWorkoutGoal: {
      type: INTEGER,
      allowNull: true,
      field: 'workout_goal'
    }

  }, {
    underscored: true,
    timestamps: false,
    freezeTableName: true
  });

    // Class methods

  Goals.make = async function (data) {
    return Goals.create(data);
  };

  return Goals;
};
