'use-strict';

const config = require('../../../config');
const values = require('lodash/values');

module.exports = function (sequelize, DataTypes) {
  const { STRING, DATE, UUID, UUIDV4 } = DataTypes;

  const Played = sequelize.define('played', {
    userId: {
      type: UUID,
      allowNull: false,
      field: 'user_id',
      primaryKey: true
    },
    videoWatched: {
      type: UUID,
      allowNull: false,
      field: 'video_watched',
      primaryKey: true
    },
    lastPlayed: {
      type: DATE,
      allowNull: false,
      field: 'last_played'
    },
    courseId: {
      type: UUID,
      allowNull: false,
      field: 'course_id'
    },
    progress: {
      type: STRING,
      allowNull: true,
      field: 'time_watched'
    }
  }, {
    underscored: true,
    timestamps: true,
    freezeTableName: true
  });

    // Class methods
  Played.removeAttribute('id');
  Played.make = async function (data) {
    return Played.create(data);
  };

  return Played;
};
