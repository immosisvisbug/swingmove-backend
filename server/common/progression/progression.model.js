'use-strict';

const config = require('../../../config');
const values = require('lodash/values');

module.exports = function (sequelize, DataTypes) {
  const { STRING, DATE, UUID, UUIDV4 } = DataTypes;

  const Progression = sequelize.define('progression', {
    userId: {
      type: UUID,
      allowNull: false,
      field: 'user_id',
      primaryKey: true
    },
    videoWatched: {
      type: UUID,
      allowNull: false,
      field: 'video_watched',
      primaryKey: true
    },
  }, {
    underscored: true,
    timestamps: true,
    freezeTableName: true
  });

    // Class methods
  Progression.removeAttribute('id');
  Progression.make = async function (data) {
    return Progression.create(data);
  };

  return Progression;
};
