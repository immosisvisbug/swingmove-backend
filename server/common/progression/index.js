'use-strict';

const ct = require('./progression.controller');
const router = require('express-promise-router')();

router
  .post('/completed', ct.completed)
  .get('/completed', ct.listCompletedWorkouts)
  .get('/latest', ct.latestWorkoutCompleted)
  .post('/watched', ct.watched)

module.exports = { router };


