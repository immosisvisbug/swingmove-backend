'use-strict';

const {
  BAD_REQUEST
} = require('http-status-codes');
const { Op } = require('sequelize');

const config = require('../../../config');
const { createHttpError } = require('../../shared/error');

const { Video, Progression, Goals, Played, Badge, Sequelize } = require('../../database');
const { listDailyCompleted} = require('../goals/goals.controller');
const moment = require('moment-timezone');
const pick = require('lodash/pick');

// TODO Add stuff to update user goals stuff
// TODO Make sure that user can still mark video as complete even if has been before
// TODO Test to make sure badges work
async function completed({ body, user }, res) {
  var { videoId } = body;
  if (!videoId) {
    await createHttpError(BAD_REQUEST, 'Video ID missing.');
  };

  let video = await Video.findOne({
    where: {
      id: videoId
    }
  });

  if (!video) {
    await createHttpError(BAD_REQUEST, 'Video with that ID does not exist');
  }

  /*
  // Mark Latest Workout Watch for User in Course

  let course = await Course.findOne({
    where: {
      id: video.courseId
    }
  });

  if (course) {

    course = await course.update({latestWorkoutWatched: videoId})
  }

  */

  await Progression.make({userId: user.id, videoWatched: videoId}).catch(async function (err) {
    let progression = await Progression.findOne({
      where: {
        userId: user.id,
        videoWatched: videoId
      }
    });

    await progression.destroy();
    await Progression.make({userId: user.id, videoWatched: videoId});

    // await createHttpError(BAD_REQUEST, 'This Video has already been marked as complete');
  });

  console.log('reached here');
  let goals = await Goals.findOne({
    where: {
      userId: user.id
    }
  });

  if (!goals) {
    goals = await Goals.make({userId: user.id, streak: 0});
  }

  // Get user's timezone
  let timeZone = user.timeZone;
  let currentDate = moment().tz(timeZone);

  const completedWorkouts = await Progression.findAll({
    where: {
      userId: user.id
    },
    order: [
      ['created_at', 'DESC'] // Sorts by COLUMN_NAME_EXAMPLE in ascending order
    ]
  });

  var goalsData = [];

  let dailyWorkouts = await completedWorkouts.filter(completed => {
    // Check
    let completedDate = moment(completed.dataValues.created_at).tz(timeZone);
    var isToday = currentDate.isSame(completedDate, 'day');

    if (isToday) {
      return completed;
    }
  });


  let dailyWorkoutsCompleted = dailyWorkouts.length;
  //console.log('workouts completed today is ', dailyWorkoutsCompleted)
  let dailyGoal = goals.dailyWorkoutGoal ? goals.dailyWorkoutGoal : 9999;

  let lastWorkoutGoal = goals.lastWorkoutGoal ? moment(goals.lastWorkoutGoal).tz(timeZone) : undefined;

  if ((!(currentDate.isSame(lastWorkoutGoal, 'day')) || lastWorkoutGoal == undefined) && dailyWorkoutsCompleted >= dailyGoal) {
    goals.update({streak: goals.dataValues.streak + 1});

    // Save Last date workout goal was completed
    goals.update({lastWorkoutGoal: currentDate});
  }

  // Track highest streak for Badges
  let badge = await Badge.findOne({
    where: {
      userId: user.id
    }
  });

  if (!badge) {
    badge = await Badge.make({userId: user.id, highestStreak: 0});
  }

  // Check if a video was played yesterday
  // Make sure that repeat videos are actually updated appropriately

  let startOfDayBefore = moment().tz(timeZone).subtract(1, 'days').toDate();
  let startOfDayToday = moment().tz(timeZone).startOf('day');

  let videos = await Progression.findAll({
    where: {
      userId: user.id,
      updated_at: {
        [Op.gte]: startOfDayBefore,
        [Op.lt]: startOfDayToday
      }
    }
  });
  // if Video exists then update streak while taking into account multiple videos watched in the same day

  // If created at is today then make sure it has not been updated already or if the badge has not been updated today

  // if the badge was created today then make sure that it has not already been updated for the day
  // if ( (moment(badge.dataValues.created_at).isSame(currentDate, 'day')  && moment(badge.dataValues.updated_at).isSame(moment(badge.dataValues.created_at), 'hour') )

  let badgeCreatedToday = moment(badge.dataValues.created_at).isSame(currentDate, 'day');
  let badgeAlreadyUpdatedToday = function () {
    if (moment(badge.dataValues.updated_at).tz(timeZone) < startOfDayToday) {
      return false;
    }

    if (badgeCreatedToday && moment(badge.dataValues.updated_at).isSame(moment(badge.dataValues.created_at))) {
      return false;
    } else {
      return true;
    }
  };


  //console.log('badge created today', badgeCreatedToday)
  //console.log('badge already updated today', badgeAlreadyUpdatedToday())

  if (videos && !badgeAlreadyUpdatedToday()) {
    goals.update({dayStreak: goals.dataValues.dayStreak + 1 });
  }
  // else if no videos then that means that their streak got broken
  else if (!(video.length > 0)) {
    console.log('here')
    goals.update({dayStreak: 1});
  }

  var newHigh = false
  // Just to track max badge streak not taking into account if the user hit their daily goal or not
  if (badge && badge.dataValues.highestStreak < goals.dataValues.dayStreak) {
    await badge.update({highestStreak: goals.dataValues.dayStreak });
    newHigh = true
  }

  console.log('we reached a new high', newHigh)
  goals.update({latestWorkoutWatched: videoId});

  res.json({ data: { msg: 'Video Marked as Complete', badges: badge.dataValues.highestStreak, newHigh: newHigh} });
}

// TODO Find out if I need the complete video information or just the id

async function listCompletedWorkouts({ user }, res) {
  const completed = await Progression.findAll({
    where: {
      userId: user.id
    }
  });

  res.json({ data: { completed: completed } });
}

async function watched({body, user }, res) {
  var {videoId} = body;
  if (!videoId) {
    await createHttpError(BAD_REQUEST, 'Video ID missing.');
  }
  ;

  let video = await Video.findOne({
    where: {
      id: videoId
    }
  });

  if (!video) {
    await createHttpError(BAD_REQUEST, 'Video with that ID does not exist');
  }

  var played = await Played.findOne({
    where: {
      userId: user.id,
      videoWatched: videoId
    }
  });

  if (!played) {
    await Played.make({userId: user.id, videoWatched: videoId, lastPlayed: new Date(), courseId: video.courseId});
  } else {
    played = await played.update({lastPlayed: new Date()});
  }
  res.json({ data: { completed: completed } });
}

async function latestWorkoutCompleted({ user }, res) {
  let goals = await Goals.findOne({
    where: {
      userId: user.id
    }
  });

  let videoId = goals.latestWorkoutWatched;

  if (!videoId) {
    await createHttpError(BAD_REQUEST, 'The user has not completed any workouts');
  }

  let video = await Video.findOne({
    where: {
      id: videoId
    }
  });

  res.json({ data: { video: video } });
}

module.exports = {
  completed,
  listCompletedWorkouts,
  latestWorkoutCompleted,
  watched
};
