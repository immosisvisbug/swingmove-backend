'use-strict';

const ct = require('./payment.controller');
const router = require('express-promise-router')();

router
  .get('/membership-price', ct.membershipPrice)
  .post('/stripe-hook', ct.stripeHook);

module.exports = { router };
