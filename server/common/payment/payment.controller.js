'use-strict';

const { BAD_REQUEST } = require('http-status-codes');
const { createHttpError } = require('../../shared/error');
const { getMembershipPrice } = require('../../shared/payment');
const toUpper = require('lodash/toUpper');
const { User, Sequelize } = require('../../database');
const config = require('../../../config');
const stripe = require('stripe')(config.stripe.secret);

async function membershipPrice({ query }, res) {
  let { coupon, membership } = query;
  if (!coupon || !membership) {
    await createHttpError(BAD_REQUEST, 'Provide coupon and subscription type.');
  }
  let price;
  try {
    price = await getMembershipPrice(toUpper(membership), coupon);
  } catch (err) {
    price = null;
  }
  res.json({ data: { price } });
}

async function stripeHook(req, res) {
  let sig = req.headers['stripe-signature'];

  let endpointSecret = config.stripe.webhook;
  try {

    //let event = stripe.webhooks.constructEvent(req.body, sig, endpointSecret);
    const eventJson = req.body;

    let type = eventJson.type;

    // Payment Failed three times so this event is called
    // https://www.masteringmodernpayments.com/stripe-webhook-event-cheatsheet

    if (type && type == 'customer.subscription.deleted') {
      let customerId = eventJson.data.object.customer;
      // Switch Customer to Free Plan
      if (customerId) {
        let user = await User.findOne({
          where: {
            stripeCustomerId: customerId
          }
        });

        if (user) {
          user.update({membership: config.membership.FREE});
        }
      }
    }

    // Do something with event
  } catch (err) {
    res.status(400).end();
  }

  res.json({received: true});
}

module.exports = { membershipPrice, stripeHook };
