'use-strict';

const multer = require('multer');
const path = require('path');
const Promise = require('bluebird');

// 0.5GB Max Video Upload size
const MAX_FILE_UPLOAD = 512 * 1024 * 1024;

let upload = multer({
  fileFilter: function (req, file, cb) {
    const errorMsg = 'File upload only supports the following filetypes: mp4, mov, avi.';
    let filetypes = /mp4|mov|avi/;
    let mimetype = filetypes.test(file.mimetype);
    let extname = filetypes.test(path.extname(file.originalname).toLowerCase());
    console.log(mimetype, extname)
    if (mimetype && extname) {
      return cb(null, true);
    }
    console.log('error', errorMsg)
    cb(new Error(errorMsg));
  },
  limits: { fileSize: MAX_FILE_UPLOAD }
}).single('video');

function uploadVideo(req, res) {
  return new Promise((resolve, reject) => {
    upload(req, res, err => {
      if (err) reject(err);
      resolve(req.file);
    });
  });
}

module.exports = { uploadVideo };
