'use-strict';

const path = require('path');
const storage = require('../../shared/storage');

const VIDEO_PATH = 'videos';

const generateKey = file => path.join(
  VIDEO_PATH, file
);

function getVideo(name) {
  console.log(name)
  const key = generateKey(name);
  return storage.getFileUrl(key);
}

async function storeVideo(file, name, type) {
  // TODO: Check if video is valid
    console.log('storing video')
    console.log(file, name, type)
  const key = generateKey(name);
  let result = await storage.saveFile(key, file, { ACL: 'public-read', ContentType: type });
  console.log(result)
    return result
}

function deleteVideo(name) {
  const key = generateKey(name);
  return storage.deleteFile(key);
}

module.exports = {
  getVideo,
  storeVideo,
  deleteVideo
};
