'use-strict';

const ct = require('./video.controller');
const router = require('express-promise-router')();

router
  .post('/:course/:title/:category', ct.addVideo)
  .get('/', ct.getVideosList)
  .delete('/:id', ct.deleteVideo);

module.exports = { router };
