'use-strict';

const config = require('../../../config');
const values = require('lodash/values');

module.exports = function (sequelize, DataTypes) {
  const { STRING, DOUBLE, UUID, UUIDV4 } = DataTypes;

  const Video = sequelize.define('video', {
    id: {
      type: UUID,
      primaryKey: true,
      defaultValue: UUIDV4
    },
    videoUrl: {
      type: STRING,
      field: 'video_url'
    },
    title: {
      type: STRING,
      field: 'title'
    },
    courseId: {
      type: UUID,
      allowNull: false,
      field: 'course_id'
    },
    videoLength: {
      type: DOUBLE,
      field: 'video_length'
    },
    category: {
      type: STRING,
      field: 'category'
    }
  }, {
    underscored: true,
    timestamps: true,
    freezeTableName: true
  });

    // Class methods

  Video.make = async function (data) {
    return Video.create(data);
  };

  return Video;
};
