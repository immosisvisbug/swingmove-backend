'use-strict';

const {
  BAD_REQUEST
} = require('http-status-codes');

const config = require('../../../config');
const { createHttpError } = require('../../shared/error');
const request = require('request-promise');
const path = require('path');
const crypto = require('crypto');
const { Video, Course, Sequelize } = require('../../database');
const {
  storeVideo,
  getVideo
} = require('./video.storage');
const { getVideoDurationInSeconds } = require('get-video-duration');

const {addUrlToVideo} = require('../../shared/video');

const { uploadVideo } = require('./fileUpload');

const processVideo = (file, userId) => {
  let { name, ext } = path.parse(file.originalname);
  const nameFormat = `${file.originalname} - ${Date.now()} - ${userId}`;

  name = crypto.createHash('md5').update(nameFormat).digest('hex');
  name = path.format({ name, ext });
  return { name, file: file.buffer, type: file.mimetype };
};

async function addVideo(req, res) {
  const {body, user, params } = req;

  let courseId = params.course;
  let title = params.title;
  let category = params.category;

  if (!(user.membership == config.membership.ADMIN)) {
    await createHttpError(BAD_REQUEST, 'Only Admin Users can access this.');
  }

  if (!courseId) {
    await createHttpError(BAD_REQUEST, 'Please enter course Id');
  }

  if (!title) {
    await createHttpError(BAD_REQUEST, 'Please enter title for video');
  }

  if (!category) {
    await createHttpError(BAD_REQUEST, 'Please enter a category for video');
  }

  let course = await Course.findOne({
    where: {
      id: courseId
    }
  });

  if (!course) {
    await createHttpError(BAD_REQUEST, 'A course with this id does not exist');
  }

  let file;
  try {
    file = await uploadVideo(req, res);
  } catch (err) {
    await createHttpError(BAD_REQUEST, err.message);
  }

  if (file) {
    let { name, file: image, type } = processVideo(file);
    await storeVideo(image, name, type);

    let videoUrl = await getVideo(name);
    let length = await getVideoDurationInSeconds(videoUrl);
    const data = {courseId: courseId, videoUrl: name, videoLength: length, title: title, category: category.toLowerCase()};
    let video = await Video.make(data);

    res.json({ data: { video: video} });
  } else {
    console.log('there is no file');
  }
  await createHttpError(BAD_REQUEST, 'Unable to upload file');
}

// Admin User function
async function getVideosList({ user, query }, res) {
  if (!(user.membership == config.membership.ADMIN || user.membership == config.membership.PAID)) {
    await createHttpError(BAD_REQUEST, 'Only Admin and Paid Users can access this.');
  }
  let { category } = query;

  var videos = [];
  if (category) {
    videos = await Video.findAll({
      where: {
        category: category.toLowerCase()
      }
    });
  } else {
    videos = await Video.findAll();
  }

  let cleanedVideos = await addUrlToVideo(videos);

  res.json({ data: { videos: cleanedVideos } });
}

// Admin User Function
// Delete a blog post based on the id
async function deleteVideo({ user, params }, res) {
  if (user.membership != config.membership.ADMIN) {
    await createHttpError(BAD_REQUEST, 'Only Admin Users can access this.');
  }
  let id = params.id;

  if (!id) {
    await createHttpError(BAD_REQUEST, 'Please provide id of the blog post to delete');
  };
  let video = await Video.findOne({
    where: {
      id: id
    }
  });

  await video.destroy();
  res.json({ data: { messages: 'Video Deleted Successfully' } });
}

module.exports = {
  addVideo,
  getVideosList,
  deleteVideo
};
