'use-strict';

const config = require('../../../config');
const values = require('lodash/values');

module.exports = function (sequelize, DataTypes) {
  const { STRING, DATE, UUID, UUIDV4, INTEGER } = DataTypes;

  const Badge = sequelize.define('badge', {
    userId: {
      type: UUID,
      allowNull: false,
      field: 'user_id',
      primaryKey: true
    },
    highestStreak: {
      type: INTEGER,
      allowNull: false,
      defaultValue: 0,
      field: 'streak'
    },

  }, {
    underscored: true,
    timestamps: true,
    freezeTableName: true
  });

    // Class methods

  Badge.make = async function (data) {
    return Badge.create(data);
  };

  return Badge;
};
