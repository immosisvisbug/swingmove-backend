'use-strict';

const {
  BAD_REQUEST
} = require('http-status-codes');

const config = require('../../../config');
const { createHttpError } = require('../../shared/error');
const moment = require('moment-timezone');

const { Badge, Progression, Sequelize } = require('../../database');

async function getBadges({ user }, res) {
  let badge = await Badge.findOne({
    where: {
      userId: user.id
    }
  });

  if (!badge) {
    badge = await Badge.make({userId: user.id, highestStreak: 0});
  }

  let highestStreak = badge.highestStreak;

  var badges = [];
  for (let index in config.badges) {
    if (config.badges[index] <= highestStreak) {
      badges.push(config.badges[index]);
    }
  }

  res.json({ data: { badges: badges } });
}

module.exports = {
  getBadges
};
