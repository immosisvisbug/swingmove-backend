'use-strict';

const ct = require('./badges.controller');
const router = require('express-promise-router')();

router
  .get('/', ct.getBadges);

module.exports = { router };
