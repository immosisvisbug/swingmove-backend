'use-strict';

const app = require('../../../app');
const { BAD_REQUEST, CREATED, FORBIDDEN, NOT_FOUND, OK } = require('http-status-codes');
const chai = require('chai');
const chaiHttp = require('chai-http');
const { createToken } = require('../../../shared/token');
const keys = require('lodash/keys');
const mail = require('../user.mail');
const omit = require('lodash/omit');
const pick = require('lodash/pick');
const random = require('lodash/random');
const { User } = require('../../../database');
const set = require('lodash/set');
const sinon = require('sinon');
const uuidv4 = require('uuid/v4');

const expect = chai.expect;
chai.use(chaiHttp);

const testUser = {
  email: 'bilbo@mordor.org',
  password: 'throwit',
  firstName: 'Bilbo'
};

describe('POST /api/v1/users/register', function () {
  before(function (done) {
    this.url = '/api/v1/users/register';
    this.fields = keys(testUser);
    done();
  });

  beforeEach(async function (done) {
    this.mailStub = sinon.stub(mail, 'sendVerificationToken');
    done();
  });

  afterEach(function (done) {
    this.mailStub.restore();
    done();
  });

  it('user should register with valid data', async function () {
    const res = await chai.request(app).post(this.url).send(testUser);
    const user = await User.findUnique(testUser.email);

    expect(res).to.be.json;
    expect(res).to.have.status(CREATED);
    expect(user).to.exist;
    expect(res.body).to.have.property('data');
    expect(res.body.data).to.have.property('user');
    expect(res.body.data.user).to.deep.equal(await user.getProfile());
    expect(this.mailStub.calledOnce).to.be.true;
  });

  it('user should provide all required fields', async function () {
    const { fields, url, user } = this;
    try {
      await chai
        .request(app)
        .post(url)
        .send(omit(user, [fields[random(fields.length - 1)]]));
      expect.fail(null, null);
    } catch ({ response: res }) {
      expect(res).to.be.json;
      expect(res).to.have.status(BAD_REQUEST);
      expect(res.body.error.message).to.be.equal(
        'Please enter email, password and first name.'
      );
      expect(this.mailStub.notCalled).to.be.true;
    }
  });

  it('user should have unique email', async function () {
    try {
      await chai.request(app).post(this.url).send(testUser);
      expect.fail(null, null);
    } catch ({ response: res }) {
      expect(this.mailStub.notCalled).to.be.true;
      expect(res).to.be.json;
      expect(res).to.have.status(BAD_REQUEST);
      expect(res.body.error.message).to.be.equal('User already exists.');
      expect(this.mailStub.notCalled).to.be.true;
    }
  });
});

describe('GET /api/v1/users/verify-account/:token', function () {
  before(async function () {
    const user = await User.findUnique(testUser.email);
    const token = createToken(pick(user, ['id', 'email']));
    this.url = `/api/v1/users/verify-account/${token}`;
  });

  it('user successfully verifies email', async function () {
    const user = await User.findUnique(testUser.email);
    expect(user.verified).to.be.false;

    console.log('Token: ', this.url);
    const res = await chai.request(app).get(this.url);
    expect(res).to.be.json;
    expect(res).to.have.status(OK);
    expect(res.body.data.user.verified).to.be.true;
  });

  it('user can\'t confirm account with invalid token', async function () {
    try {
      await chai.request(app).get(`${this.url}_jibberish`);
      expect.fail(null, null);
    } catch ({ response: res }) {
      expect(res).to.be.json;
      expect(res).to.have.status(BAD_REQUEST);
      expect(res.body.error.message).to.be.equal('Invalid token.');
    }
  });

  it('user can\'t verify already verified email', async function () {
    const user = await User.findUnique(testUser.email);
    expect(user.verified).to.be.true;
    try {
      await chai.request(app).get(this.url);
      expect.fail(null, null);
    } catch ({ response: res }) {
      expect(res).to.be.json;
      expect(res).to.have.status(FORBIDDEN);
      expect(res.body.error.message).to.be.equal('Email already verified.');
    }
  });
});

describe('POST /api/v1/users/login', function () {
  before(function (done) {
    this.url = '/api/v1/users/login';
    this.credentials = pick(testUser, ['email', 'password']);
    done();
  });

  beforeEach(function (done) {
    this.authSpy = sinon.spy(User.prototype, 'authenticate');
    done();
  });

  afterEach(function (done) {
    this.authSpy.restore();
    done();
  });

  it('user can login with correct credentials', async function () {
    const user = await User.findUnique(testUser.email);
    const res = await chai.request(app).post(this.url).send(this.credentials);
    expect(res).to.be.json;
    expect(res).to.have.status(OK);
    expect(res.body).to.have.property('data');
    expect(res.body.data).to.have.property('user');
    expect(res.body.data).to.have.property('token');
    expect(res.body.data.user).to.deep.equal(await user.getProfile());
    expect(this.authSpy.calledOnce).to.be.true;
  });

  it('user didn\'t enter credentials', async function () {
    try {
      await chai
        .request(app)
        .post(this.url)
        .send({ email: '', password: '' });
      expect.fail(null, null);
    } catch ({ response: res }) {
      expect(res).to.be.json;
      expect(res).to.have.status(BAD_REQUEST);
      expect(res.body.error.message).to.be.equal(
        'Please enter email and password.'
      );
      expect(this.authSpy.notCalled).to.be.true;
    }
  });

  it('user didn\'t register', async function () {
    const wrongCredentials = { ...this.credentials, email: 'swagruman@isengard.com' };
    try {
      await chai.request(app).post(this.url).send(wrongCredentials);
      expect.fail(null, null);
    } catch ({ response: res }) {
      expect(res).to.be.json;
      expect(res).to.have.status(NOT_FOUND);
      expect(res.body.error.message).to.be.equal('User not found.');
      expect(this.authSpy.notCalled).to.be.true;
    }
  });

  it('user entered wrong password', async function () {
    const wrongCredentials = { ...this.credentials, password: 'tatoes' };
    try {
      await chai.request(app).post(this.url).send(wrongCredentials);
      expect.fail(null, null);
    } catch ({ response: res }) {
      expect(res).to.be.json;
      expect(res).to.have.status(FORBIDDEN);
      expect(res.body.error.message).to.be.equal('Wrong password.');
      expect(this.authSpy.calledOnce).to.be.true;
    }
  });
});

describe('POST /api/v1/users/request-reset-password', function () {
  before(function (done) {
    this.url = '/api/v1/users/request-reset-password';
    done();
  });

  beforeEach(async function (done) {
    this.mailStub = sinon.stub(mail, 'sendResetToken');
    done();
  });

  afterEach(function (done) {
    this.mailStub.restore();
    done();
  });

  it('user is sent password reset email', async function () {
    const res = await chai.request(app).post(this.url).send({ email: testUser.email });
    const user = await User.findUnique(testUser.email);
    expect(res).to.be.json;
    expect(res).to.have.status(OK);
    expect(res.body).to.have.property('data');
    expect(res.body.data).to.have.property('user');
    expect(res.body.data.user).to.deep.equal(await user.getProfile());
    expect(this.mailStub.calledOnce).to.be.true;
  });

  it('wrong email input returns empty user', async function () {
    const res = await chai
      .request(app)
      .post(this.url)
      .send({ email: 'gimli@moria.net' });
    expect(res).to.be.json;
    expect(res).to.have.status(OK);
    expect(res.body).to.have.property('data');
    expect(res.body.data).to.have.property('user');
    expect(res.body.data.user).to.be.null;
    expect(this.mailStub.notCalled).to.be.true;
  });
});

describe('POST /api/v1/users/reset-password/:token', function () {
  before(async function () {
    const user = await User.findUnique(testUser.email);
    const authToken = createToken(pick(user, ['id', 'email']));
    this.createUrl = (token = authToken) => `/api/v1/users/reset-password/${token}`;
    this.password = 'smiguel';
  });

  beforeEach(function (done) {
    this.resetPassSpy = sinon.spy(User.prototype, 'resetPassword');
    done();
  });

  afterEach(function (done) {
    this.resetPassSpy.restore();
    done();
  });

  it('user successfully changed password', async function () {
    const passwords = { password: this.password, confirmPassword: this.password };
    const res = await chai.request(app).post(this.createUrl()).send(passwords);
    const user = await User.findUnique(testUser.email);
    const authWithOldPass = await user.authenticate(testUser.password);
    const authWithNewPass = await user.authenticate(this.password);

    expect(res).to.be.json;
    expect(res).to.have.status(OK);
    expect(res.body).to.have.property('data');
    expect(res.body.data).to.have.property('user');
    expect(res.body.data.user).to.deep.equal(await user.getProfile());
    expect(authWithOldPass).to.be.false;
    expect(authWithNewPass).to.deep.equal(user);
    expect(this.resetPassSpy.calledOnce).to.be.true;
  });

  it('user failed to enter password and confirm password', async function () {
    let passwords = { password: this.password, confirmPassword: this.password };
    set(passwords, keys(passwords)[random(1)], '');
    try {
      await chai.request(app).post(this.createUrl()).send(passwords);
      expect.fail(null, null);
    } catch ({ response: res }) {
      expect(res).to.be.json;
      expect(res).to.have.status(BAD_REQUEST);
      expect(res.body.error.message).to.be.equal(
        'Please enter password and Confirm password.'
      );
      expect(this.resetPassSpy.notCalled).to.be.true;
    }
  });

  it('user didn\'t enter same password and confirm password', async function () {
    const passwords = { password: this.password, confirmPassword: 'smeagol' };
    try {
      await chai.request(app).post(this.createUrl()).send(passwords);
      expect.fail(null, null);
    } catch ({ response: res }) {
      expect(res).to.be.json;
      expect(res).to.have.status(BAD_REQUEST);
      expect(res.body.error.message).to.be.equal(
        'Password should match Confirmed password.'
      );
      expect(this.resetPassSpy.notCalled).to.be.true;
    }
  });

  it('user sent invalid token', async function () {
    const passwords = { password: this.password, confirmPassword: this.password };
    try {
      await chai.request(app).post(this.createUrl('jibberish')).send(passwords);
      expect.fail(null, null);
    } catch ({ response: res }) {
      expect(res).to.be.json;
      expect(res).to.have.status(BAD_REQUEST);
      expect(res.body.error.message).to.be.equal('Invalid token.');
      expect(this.resetPassSpy.notCalled).to.be.true;
    }
  });

  it('user doesn\'t exist', async function () {
    const passwords = { password: this.password, confirmPassword: this.password };
    const token = createToken({ id: uuidv4(), email: 'gollum@precious.xyz' });
    try {
      await chai.request(app).post(this.createUrl(token)).send(passwords);
      expect.fail(null, null);
    } catch ({ response: res }) {
      expect(res).to.be.json;
      expect(res).to.have.status(NOT_FOUND);
      expect(res.body.error.message).to.be.equal('User not found.');
      expect(this.resetPassSpy.notCalled).to.be.true;
    }
  });
});

describe('GET /api/v1/users/validate/:email', function () {
  before(function (done) {
    this.createUrl = email => `/api/v1/users/validate/${email}`;
    done();
  });

  it('users can register with provided email', async function () {
    const res = await chai.request(app).get(this.createUrl('treebeard@forest.org'));
    expect(res).to.be.json;
    expect(res).to.have.status(OK);
    expect(res.body).to.have.property('data');
    expect(res.body.data).to.have.property('valid');
    expect(res.body.data.valid).to.be.true;
  });

  it('user cannot register with email because it\'s taken', async function () {
    const res = await chai.request(app).get(this.createUrl(testUser.email));
    expect(res).to.be.json;
    expect(res).to.have.status(OK);
    expect(res.body).to.have.property('data');
    expect(res.body.data).to.have.property('valid');
    expect(res.body.data.valid).to.be.false;
  });
});

describe('GET /api/v1/users/referred-by/:code', function () {
  before(async function () {
    const user = await User.findUnique(testUser.email);
    this.user = await user.getProfile();
    this.createUrl = code => `/api/v1/users/referred-by/${code}`;
  });

  it('users referral exists', async function () {
    const res = await chai.request(app).get(this.createUrl(this.user.referral.code));
    expect(res).to.be.json;
    expect(res).to.have.status(OK);
    expect(res.body).to.have.property('data');
    expect(res.body.data).to.deep.equal(pick(this.user, ['firstName', 'profilePicture']));
  });

  it('users referral doesn\'t exists', async function () {
    try {
      await chai.request(app).get(this.createUrl('PRECIOUS-1'));
      expect.fail(null, null);
    } catch ({ response: res }) {
      expect(res).to.be.json;
      expect(res).to.have.status(NOT_FOUND);
      expect(res.body.error.message).to.be.equal('User not found.');
    }
  });
});
