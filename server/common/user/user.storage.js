'use-strict';

const { fetchProfilePicture } = require('../../shared/facebook');
const path = require('path');
const storage = require('../../shared/storage');

const USER_PATH = 'users';
const PROFILE_PICTURE_PATH = 'profile-picture';

const generateKey = file => path.join(
  USER_PATH, PROFILE_PICTURE_PATH, file
);

function getProfilePicture(name) {
  const key = generateKey(name);
  return storage.getFileUrl(key);
}

function storeProfilePicture(file, name, type) {
  // TODO: Check if image is valid
  const key = generateKey(name);
  return storage.saveFile(key, file, { ACL: 'public-read', ContentType: type });
}

function deleteProfilePicture(name) {
  const key = generateKey(name);
  return storage.deleteFile(key);
}

async function storeFacebookProfilePicture(url) {
  const { file, name, type } = await fetchProfilePicture(url);
  await storeProfilePicture(file, name, type);
  return name;
}

module.exports = {
  deleteProfilePicture,
  getProfilePicture,
  storeProfilePicture,
  storeFacebookProfilePicture
};
