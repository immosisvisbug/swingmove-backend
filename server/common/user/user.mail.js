'use-strict';

const config = require('../../../config');
const { send } = require('../../shared/mail');

const {sendMandrill, subscribeMailChimp, unSubscribeMailChimp} = require('../../shared/mail');

function sendVerificationToken(email, token, transporter = sendMandrill) {
  // NOTE: Should go to the frontend url first
  let link = `${config.client.url}/emailVerify?token=${token}`;

  // TODO: Better token logging in development
  if (config.env === 'development') {
    console.log('\n==================== TOKEN ====================\n', token);
  }

  var message = {
    from: config.email.address,
    to: email,
    subject: 'SwingMove - Confirm Account',
    text: 'Please confirm your account',
    html: '<p>Please confirm your account by clicking   :' + '<a href= "' + link + '"> Here</a></p>'
  };
  send(message);

  /*
  return transporter({
    to: email,
    subject: 'SwingMove - Confirm Account',
    template: 'email-verification',
    variables: [{ verifyurl: link }]
  }).catch(error => { throw new Error(error); });
  */
}

function sendResetToken(email, token, transporter = sendMandrill) {
  // NOTE: Should go to the frontend url first
  let link = `${config.client.url}/changepassword?token=${token}`;

  // TODO: Better token logging in development
  if (config.env === 'development') {
    console.log('\n==================== TOKEN ====================\n', token);
  }

  var message = {
    from: config.email.address,
    to: email,
    subject: 'SwingMove - Reset Password',
    text: 'Please confirm your account',
    html: '<p>Reset your password by clicking here   :' + '<a href="' + link + '"> Here</a></p>'
  };
  send(message);

  /*
  return transporter({
    to: email,
    subject: 'SwingMove - Reset Password',
    template: 'password-reset',
    variables: [{ RESETURL: link }]
  }).catch(error => { throw new Error(error); });
  */
}

function addToMailChimpList(user, list, variables, transporter = subscribeMailChimp) {
  return transporter(user, list, variables).catch(error => { throw new Error(error); });
}

function deleteFromMailChimpList(email, list, transporter = unSubscribeMailChimp) {
  return transporter(email, list).catch(error => { throw new Error(error); });
}

module.exports = { addToMailChimpList, deleteFromMailChimpList, sendResetToken, sendVerificationToken };
