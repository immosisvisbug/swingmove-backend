'use-strict';

const {
  BAD_REQUEST,
  CREATED,
  FORBIDDEN,
  MOVED_TEMPORARILY,
  NOT_FOUND,
  UNAUTHORIZED
} = require('http-status-codes');
const config = require('../../../config');
const { createHttpError } = require('../../shared/error');
const { createToken, verifyToken } = require('../../shared/token');
const crypto = require('crypto');
const path = require('path');
const pick = require('lodash/pick');
const { User, Message } = require('../../database');
const size = require('lodash/size');
const {
  deleteProfilePicture,
  storeProfilePicture
} = require('./user.storage');
const toUpper = require('lodash/toUpper');
const { uploadProfilePicture } = require('./fileUpload');
const mail = require('./user.mail');

const { getNextBillingDate } = require('../../shared/payment');

const {verifyFb} = require('../../auth');
var requestpromise = require('request-promise');
const get = require('lodash/get');

const processImage = (file, userId) => {
  let { name, ext } = path.parse(file.originalname);
  const nameFormat = `${file.originalname} - ${Date.now()} - ${userId}`;

  name = crypto.createHash('md5').update(nameFormat).digest('hex');
  name = path.format({ name, ext });
  return { name, file: file.buffer, type: file.mimetype };
};

// TODO: Test
async function show(req, res) {

  let billingDate = await getNextBillingDate(req.user)
  var profile = await req.user.getProfile()
  profile.nextBillingDate = billingDate
  res.json({ data: { user: profile } });
}

async function deleteUser({params, user}, res) {
  let id = params.id;

  if (user.membership != config.membership.ADMIN) {
    await createHttpError(BAD_REQUEST, 'Only Admin Users can access this.');
  }

  let userToDelete = await User.findById(id);

  if (userToDelete.membership == config.membership.ADMIN) {
    await createHttpError(BAD_REQUEST, 'You cannot delete an Admin user');
  }

  if (userToDelete) {
    let deleted = await userToDelete.destroy();
    res.json({ data: { deleted: deleted} });
  } else {
    await createHttpError(BAD_REQUEST, 'User does not exist');
  }
}

// TODO: Test
async function patch(req, res) {
  let { body, user } = req;
  const data = pick(body, ['email', 'firstName', 'lastName', 'timeZone']);
  const membership = config.membership[toUpper(body.membership)] || user.membership;
  const plan = config.plans[toUpper(body.plan)] || undefined;

  if (plan || user.membership !== membership) {
    var { coupon, paymentSource } = body;

    var trialDays = 0;

    if (plan) {

      console.log('plan')
      if (plan.toLowerCase() == 'monthly' && coupon && coupon.toLowerCase() == 'free_trial') {
        trialDays = 60;
        coupon = undefined
        console.log('trial days?')
      } else if (plan.toLowerCase() == 'yearly' && coupon && coupon.toLowerCase() == 'free_trial') {
        trialDays = 60;
        coupon = undefined
      }
    }

    user = await user.changeMembership(plan || membership, paymentSource, coupon, trialDays);
  }

  let file;
  try {
    file = await uploadProfilePicture(req, res);
  } catch (err) {
    await createHttpError(BAD_REQUEST, err.message);
  }

  if (file) {
    if (user.profilePicture) deleteProfilePicture(user.profilePicture);
    let { name, file: image, type } = processImage(file);
    await storeProfilePicture(image, name, type);
    data.profilePicture = name;
  }

  console.log(data);
  user = await user.update(data);
  res.json({ data: { user: await user.getProfile() } });
}

async function register({ body }, res) {
  const fields = ['email', 'password', 'firstName', 'lastName', 'timeZone'];
  const { password, passwordConfirmation } = body;

  let data = pick(body, fields);
  if (size(data) !== fields.length) {
    await createHttpError(
      BAD_REQUEST,
      'Please enter email, password and first name and timeZone.'
    );
  }

  if (password != passwordConfirmation) {
    await createHttpError(
      BAD_REQUEST,
      'Make sure password matches.'
    );
  }

  if (password && password.length < 6) {
    await createHttpError(
      BAD_REQUEST,
      'Password should contain more than 6 characters.'
    );
  }

  // const user = await User.make(data);

  // TODO Check if this needs to be done
  var user = await User.sendEmailVerification({ ...data });

  // TODO to see how they are added to the list
  // Add them to mailchimp list

  if (user.membership === config.membership.FREE) {
    try {
      const variables = {
      };
      await User.addUserToList(user, 'defaultList', variables);
    } catch (e) {
      console.error(e);
    }
  }

  let token = createToken(pick(user, ['id', 'email']), { expiresIn: '5 days' });

  res.status(CREATED).json({ data: { user: await user.getProfile(), token: token } });
}

async function verifyAccount({ params }, res) {
  const { email } = await User.verifyToken(params.token);
  const user = await User.verifyEmail(email);
  res.json({ data: { user: await user.getProfile() } });
}

// Admin User function
async function listUsers({ user }, res) {
  if (user.membership != config.membership.ADMIN) {
    await createHttpError(BAD_REQUEST, 'Only Admin Users can access this.');
  }
  const allUsersTemp = await User.findAll();

  var allUsers = [];

  for (let index in allUsersTemp) {
    var tempUser = allUsersTemp[index];
    tempUser = await tempUser.getProfile();
    allUsers.push(tempUser);
  }

  res.json({ data: { user: await allUsers } });
}

async function login({ body }, res) {
  const { email, password } = body;
  if (!email || !password) {
    await createHttpError(BAD_REQUEST, 'Please enter email and password.');
  };

  let user = await User.findUnique(email.toLowerCase());
  if (!user) await createHttpError(NOT_FOUND, 'User not found.');

  user = await user.authenticate(password);
  if (!user) await createHttpError(FORBIDDEN, 'Wrong password.');
  // if (!user.verified) await createHttpError(FORBIDDEN, 'You need to verify your email.');

  let token = createToken(pick(user, ['id', 'email']), { expiresIn: '5 days' });
  let cookieOptions = {
    domain: config.client.domain,
    httpOnly: false,
    maxAge: 9000000000
  };
  res.cookie('jwt', token, cookieOptions);
  res.json({ data: { user: await user.getProfile(), token: token} });
}

async function loginWithToken(req, res) {
  const { token } = req.body;
  if (!token) {
    await createHttpError(BAD_REQUEST, 'No token given.');
  };

  try {
    let result = await verifyToken(token);

    res.cookie('jwt', token);
    res.json({ data: { result } });
  } catch (e) {
    await createHttpError(UNAUTHORIZED, e.message);
  }
}

async function logout(req, res) {
  // TODO: Deauthorize jwt on server side
  res.json({ message: 'In progress' });
}

async function requestResetPassword({ body }, res) {
  const { email } = body;
  const user = await User.findUnique(email.toLowerCase());
  console.log(user);
  var error = null;
  if (user) {
    await user.sendPasswordReset().catch(err => { error = err; });
    res.json({ data: { user: await user.getProfile() } });
  } else {
    await createHttpError(BAD_REQUEST, 'User does not exist');
  }

  if (error) {
    await createHttpError(BAD_REQUEST, error);
  }
}

async function resendVerification({ body, params }, res) {
  const { email } = body;

  if (!email) {
    await createHttpError(
      BAD_REQUEST,
      'Please enter email to send verification for.'
    );
  }

  const user = await User.findUnique(email.toLowerCase());

  if (!user) await createHttpError(BAD_REQUEST, 'User with this email does not exist.');

  const token = createToken(pick(user, ['id', 'email']), { expiresIn: '7d' });
  await mail.sendVerificationToken(user.email, token);

  res.json({ data: { user: await user.getProfile() } });
}

async function resetPassword({ body, params }, res) {
  const { password, confirmPassword } = body;
  if (!password || !confirmPassword) {
    await createHttpError(
      BAD_REQUEST,
      'Please enter password and Confirm password.'
    );
  }

  if (password !== confirmPassword) {
    await createHttpError(
      BAD_REQUEST,
      'Password should match Confirmed password.'
    );
  }

  const { id } = await User.verifyToken(params.token);
  const user = await User.findById(id);
  if (!user) await createHttpError(NOT_FOUND, 'User not found.');

  await user.resetPassword(password);
  res.json({ data: { user: await user.getProfile() } });
}

// TODO: Revise sending JWT to client
// TODO 2.0: Write tests
async function facebookLogin(req, res) {
  const token = createToken(pick(req.user, ['id', 'email']));
  const { url: host } = config.client;
  console.log(config.facebook);
  const { clientRedirectUrl: redirect } = config.facebook;
  const redirectUrl = `${host}${redirect}?token=${token}&userId=${req.user.id}`;
  let cookieOptions = {
    domain: config.client.domain,
    httpOnly: false,
    maxAge: 9000000000
  };
  res.cookie('jwt', token, cookieOptions);
  res.redirect(MOVED_TEMPORARILY, redirectUrl);
}

async function facebookLoginWithToken({query}, res) {
  // Grab access token after client logs in
  const token = query.token;

  // Call Graph api and get user details
  const response = await requestpromise(
    `https://graph.facebook.com/me?access_token=${token}&fields=id,first_name,last_name,email,picture.width(200).height(200)`
  );

  const {
    id: facebookId,
    first_name: firstName,
    last_name: lastName,
    email,
    picture
  } = await JSON.parse(response);

    // Check if user exists or not and do something appropriately
  const data = {
    facebookId,
    firstName,
    lastName,
    email,
    pictureUrl: get(picture, 'data.url')
  };

  try {
    console.log(data);
    const user = await User.findOrCreateFbUser(data);
    let temp_token = createToken(pick(user, ['id', 'email']), { expiresIn: '5 days' });
    res.json({ data: { user: await user.getProfile(), token: temp_token} });
  } catch (err) {
    res.json({ error: err});
  }
}

async function validate({ params }, res) {
  const valid = !(await User.findUnique(params.email));
  res.json({ data: { valid } });
}

module.exports = {
  show,
  patch,
  login,
  logout,
  register,
  requestResetPassword,
  resetPassword,
  verifyAccount,
  facebookLogin,
  validate,
  loginWithToken,
  listUsers,
  deleteUser,
  facebookLoginWithToken,
  resendVerification
};
