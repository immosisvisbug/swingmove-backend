'use-strict';

const ct = require('./user.controller');
const router = require('express-promise-router')();

router
  .get('/list', ct.listUsers)
  .get('/', ct.show)
  .patch('/', ct.patch)
  .post('/login', ct.login)
  .post('/register', ct.register)
  .post('/request-reset-password', ct.requestResetPassword)
  .post('/reset-password/:token', ct.resetPassword)
  .get('/verify-account/:token', ct.verifyAccount)
  .get('/fb/login', ct.facebookLogin)
  .get('/validate/:email', ct.validate)
  .post('/token', ct.loginWithToken)
  .delete('/:id', ct.deleteUser)
  .get('/loginWithTokenfb', ct.facebookLoginWithToken)
  .post('/resend-verification', ct.resendVerification)

module.exports = { router };
