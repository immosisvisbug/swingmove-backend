'use-strict';

const Promise = require('bluebird');
const { User } = require('../../../database');
const userData = require('./users.json');

async function seed() {
  return Promise.all(userData.map(data => {
    return User.make(data);
  }));
}

module.exports = seed;
