'use-strict';

const { BAD_REQUEST, FORBIDDEN } = require('http-status-codes');
const bcrypt = require('bcrypt');
const request = require('request');
const config = require('../../../config');
const { createHttpError } = require('../../shared/error');
const { createCustomer, changeSubscription } = require('../../shared/payment');
const { createToken } = require('../../shared/token');
const get = require('lodash/get');
const {
  getProfilePicture,
  storeFacebookProfilePicture
} = require('./user.storage');
const jwt = require('jsonwebtoken');
const mail = require('./user.mail');
const omit = require('lodash/omit');
const pick = require('lodash/pick');
const values = require('lodash/values');
const toUpper = require('lodash/toUpper');

// TODO: Add token blacklist storage!
module.exports = function (sequelize, DataTypes) {
  const { BOOLEAN, DATE, ENUM, STRING, UUID, UUIDV4 } = DataTypes;
  const User = sequelize.define('user', {
    id: {
      type: UUID,
      primaryKey: true,
      defaultValue: UUIDV4
    },
    facebookId: {
      type: STRING,
      unique: true,
      field: 'facebook_id'
    },
    stripeCustomerId: {
      type: STRING,
      unique: true,
      field: 'stripe_customer_id'
    },
    firstName: {
      type: STRING,
      validate: { notEmpty: true, len: [0, 50] },
      allowNull: false,
      field: 'first_name'
    },
    middleName: {
      type: STRING,
      validate: { len: [0, 50] },
      field: 'middle_name'
    },
    lastName: {
      type: STRING,
      validate: { len: [0, 50] },
      field: 'last_name'
    },
    email: {
      type: STRING,
      validate: { notEmpty: true, isEmail: true },
      allowNull: false,
      unique: { msg: 'The specified email address is already in use.' }
    },
    password: {
      type: STRING,
      validate: { notEmpty: true, len: [3, 100] }
    },
    verified: {
      type: BOOLEAN,
      validate: { notEmpty: true },
      allowNull: false,
      defaultValue: false
    },
    profilePicture: {
      type: STRING,
      validate: { notEmpty: true },
      allowNull: true,
      field: 'profile_picture'
    },
    membership: {
      type: ENUM(values(config.membership)),
      defaultValue: config.membership.FREE
    },
    plan: {
      type: ENUM(values(config.plans)),
      defaultValue: config.plans.FREE
    },
    timeZone: {
      type: STRING,
      allowNull: true
    },
    createdAt: {
      type: DATE,
      field: 'created_at'
    },
    updatedAt: {
      type: DATE,
      field: 'updated_at'
    }
  }, {
    getterMethods: {
      profile() {
        const fields = [
          'id',
          'firstName',
          'lastName',
          'email',
          'profilePicture',
          'verified',
          'membership',
          'plan',
          'createdAt'
        ];
        return pick(this, fields);
      }
    },
    hooks: {
      beforeCreate(user) {
        return user.encryptPassword();
      },
      beforeUpdate(user) {
        return user.changed('password')
          ? user.encryptPassword()
          : Promise.resolve();
      },
      beforeBulkCreate(users) {
        let updates = [];
        users.forEach(user => updates.push(user.encryptPassword()));
        return Promise.all(updates);
      }
    },
    underscored: true,
    timestamps: true,
    freezeTableName: true
  });

  // Class methods
  User.findUnique = function (email) {
    const { Sequelize: db } = sequelize;
    return User.find({
      where: db.where(db.fn('lower', db.col('email')), db.fn('lower', email))
    });
  };

  User.make = async function (data) {
    const userData = data;

    if (await User.findUnique(userData.email)) {
      return createHttpError(BAD_REQUEST, 'User already exists.');
    }

    // Remove white spaces
    userData.firstName = userData.firstName.replace(/\s/g, '');
    const user = await User.create(userData);

    return user;
  };

  User.verifyToken = async function (token) {
    try {
      return jwt.verify(token, config.auth.jwtSecret);
    } catch (err) {
      return createHttpError(BAD_REQUEST, 'Invalid token.');
    }
  };

  User.sendEmailVerification = async function (data) {
    const user = await User.make(data);
    //const token = createToken(pick(user, ['id', 'email']), { expiresIn: '7d' });
    //await mail.sendVerificationToken(user.email, token);
    return user;
  };

  User.verifyEmail = async function (email) {
    const user = await User.findUnique(email);
    if (user.verified) return createHttpError(FORBIDDEN, 'Email already verified.');

    // Remove all future scheduled email verification emails
    let url = 'https://mandrillapp.com/api/1.0/';
    request.post(
      {
        url: url + 'messages/list-scheduled',
        form: { key: config.email.mandrill, to: email }
      },
      (err, httpResponse, body) => {
        let response = JSON.parse(body);
        let length = response.length;
        for (var i = 0, l = length; i < l; i++) {
          if (response[i].to == user.email) {
            request.post(
              {
                url: url + 'messages/cancel-scheduled',
                form: { key: config.email.mandrill, id: response[i]._id }
              },
              (err, httpResponse, body) => {}
            );
          }
        }
      }
    );
    user.verified = true;
    return user.save();
  };

  User.findOrCreateFbUser = async function (data) {
    const query = { where: {
      [sequelize.Sequelize.Op.or]: [
        { facebookId: data.facebookId },
        { email: data.email }
      ]
    } };
    const user = await User.findOne(query);

    if (!user) {
      data.profilePicture = await storeFacebookProfilePicture(data.pictureUrl);
      return User.make({ ...data, verified: true });
    } else {
      if (!user.profilePicture && user.facebookId) {
        const imageName = await storeFacebookProfilePicture(data.pictureUrl);
        await user.update({ profilePicture: imageName });
        return user;
      }
      return user.update({ facebookId: data.facebookId });
    }
  };

  User.addUserToList = async function (user, list, variables) {
    await mail.addToMailChimpList(user, list, variables);
    return user;
  };

  User.deleteUserFromList = async function (email, list) {
    const user = await User.findUnique(email);
    await mail.deleteFromMailChimpList(email, list);
    return user;
  };

  // Instance methods
  User.prototype.authenticate = function (password) {
    if (!this.password) return Promise.resolve(false);
    return bcrypt
      .compare(password, this.password)
      .then(match => match ? this : false);
  };

  User.prototype.encrypt = function (val) {
    return bcrypt.hash(val, config.auth.saltRounds);
  };

  User.prototype.encryptPassword = function () {
    if (!this.password) return Promise.resolve(false);
    return this
      .encrypt(this.password)
      .then(pw => (this.password = pw));
  };

  User.prototype.sendPasswordReset = async function () {
    const token = createToken(pick(this, 'id', 'email'), { expiresIn: '15m' });
    await mail.sendResetToken(this.email, token);
    return this;
  };

  User.prototype.resetPassword = async function (password) {
    this.setDataValue('password', password);
    return this.save();
  };

  User.prototype.getProfile = async function () {
    let profile = { ...this.profile};
    if (this.profilePicture) {
      profile.profilePicture = await getProfilePicture(this.profilePicture);
    };
    return profile;
  };

  User.prototype.changeMembership = async function (type, source, coupon, trialDays) {
    let user = { membership: type == config.membership.FREE ? config.membership.FREE : config.membership.PAID, plan: type == config.plans.FREE ? config.plans.FREE : type };

    try {
      let { stripeCustomerId: customerId } = this;
      if (!customerId) {
        customerId = get(await createCustomer(this), 'id');
        user.stripeCustomerId = customerId;
      }

      await changeSubscription(customerId, type, source, coupon, trialDays);
    } catch (err) {
      await createHttpError(err.statusCode, err.message);
    }
    return this.update(user);
  };

  return User;
};
