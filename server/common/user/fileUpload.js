'use-strict';

const multer = require('multer');
const path = require('path');
const Promise = require('bluebird');

const MAX_FILE_UPLOAD = 3 * 1024 * 1024;

let upload = multer({
  fileFilter: function (req, file, cb) {
    const errorMsg = 'File upload only supports the following filetypes: jpeg, jpg, png.';
    let filetypes = /jpeg|jpg|png/;
    let mimetype = filetypes.test(file.mimetype);
    let extname = filetypes.test(path.extname(file.originalname).toLowerCase());

    if (mimetype && extname) {
      return cb(null, true);
    }
    cb(new Error(errorMsg));
  },
  limits: { fileSize: MAX_FILE_UPLOAD }
}).single('profilePicture');

function uploadProfilePicture(req, res) {
  return new Promise((resolve, reject) => {
    upload(req, res, err => {
      if (err) reject(err);
      resolve(req.file);
    });
  });
}

module.exports = { uploadProfilePicture };
