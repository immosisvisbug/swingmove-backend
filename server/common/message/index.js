'use-strict';

const ct = require('./message.controller');
const router = require('express-promise-router')();

router
  .post('/', ct.contactAdmin)
  .get('/list', ct.listMessages)
  .post('/newsletter', ct.addAnonToNewsletter)
  .delete('/:id', ct.deleteMessage);

module.exports = { router };
