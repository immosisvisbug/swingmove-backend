'use-strict';

const {
  BAD_REQUEST
} = require('http-status-codes');

const config = require('../../../config');
const { createHttpError } = require('../../shared/error');
const request = require('request-promise');
const { send } = require('../../shared/mail');

const { Message, User, Sequelize } = require('../../database');

async function contactAdmin({ body, user }, res) {
  var { title, description } = body;
  if (!title || !description) {
    await createHttpError(BAD_REQUEST, 'Please enter title and description.');
  };

  description += '\n from: ' + user.email;

  var message = {
    from: user.email,
    to: config.mail.address,
    subject: title,
    text: description,
    html: '<p>' + description + '</p>'
  };
  send(message);

  await Message.make({userId: user.id, title: title, description: description});

  res.json({ data: { msg: 'Message Received'} });
}

// Admin Functionality
async function deleteMessage({ params, user }, res) {
  if (user.membership != config.membership.ADMIN) {
    await createHttpError(BAD_REQUEST, 'Only Admin Users can access this.');
  }

  let id = params.id;

  if (!id) {
    await createHttpError(BAD_REQUEST, 'Please provide id of the message to delete');
  };

  let message = await Message.findOne({
    where: {
      id: id
    }
  });

  if (!message) {
    await createHttpError(BAD_REQUEST, 'A message with this id does not exist');
  };

  await message.destroy();

  res.json({ data: { message: 'Message Deleted' } });
}

// Admin User function
async function listMessages({ user }, res) {
  if (user.membership != config.membership.ADMIN) {
    await createHttpError(BAD_REQUEST, 'Only Admin Users can access this.');
  }
  const messages = await Message.findAll();

  res.json({ data: { messages: messages } });
}

async function addAnonToNewsletter({body}, res) {
  const {email, firstName, lastName} = body;

  if (!email || !firstName) {
    await createHttpError(BAD_REQUEST, 'Please enter email and first name of user to add .');
  };
  const user = {
    email: email,
    firstName: firstName,
    lastName: lastName
  };

  await User.addUserToList(user, 'defaultList', {});

  res.json({ data: { msg: 'User added to Newsletter List' } });
}

module.exports = {
  contactAdmin,
  listMessages,
  addAnonToNewsletter,
  deleteMessage
};
