'use-strict';

const config = require('../../../config');
const values = require('lodash/values');

module.exports = function (sequelize, DataTypes) {
  const { STRING, DATE, UUID, UUIDV4 } = DataTypes;

  const Message = sequelize.define('message', {
    id: {
      type: UUID,
      primaryKey: true,
      defaultValue: UUIDV4
    },
    title: {
      type: STRING,
      field: 'title'
    },
    description: {
      type: STRING,
      field: 'description'
    },
    userId: {
      type: UUID,
      allowNull: false,
      field: 'user_id'
    },
  }, {
    underscored: true,
    timestamps: true,
    freezeTableName: true
  });

    // Class methods

  Message.make = async function (data) {
    return Message.create(data);
  };

  return Message;
};
