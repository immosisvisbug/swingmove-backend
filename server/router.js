'use-strict';

const router = require('express-promise-router')();
const userRouter = require('./common/user').router;
const messageRouter = require('./common/message').router;
const postRouter = require('./common/post').router;
const videoRouter = require('./common/video').router;
const goalsRouter = require('./common/goals').router;
const progressionRouter = require('./common/progression').router;
const notificationRouter = require('./common/notification').router;
const courseRouter = require('./common/course').router;
const badgeRouter = require('./common/badges').router;
const paymentRouter = require('./common/payment').router;



router.use('/users', userRouter);
router.use('/message', messageRouter);
router.use('/post', postRouter);
router.use('/video', videoRouter);
router.use('/goals', goalsRouter);
router.use('/progression', progressionRouter);
router.use('/notification', notificationRouter);
router.use('/course', courseRouter);
router.use('/badge', badgeRouter);
router.use('/payment', paymentRouter);


module.exports = router;
