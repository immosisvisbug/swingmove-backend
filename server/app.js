'use-strict';

// Load env vars
const dotenv = require('dotenv');
const dotenvExpand = require('dotenv-expand');

dotenvExpand(dotenv.config());

const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const config = require('../config');
const cors = require('cors');
const express = require('express');
const includes = require('lodash/includes');
const { INTERNAL_SERVER_ERROR, NO_CONTENT, NOT_FOUND } = require('http-status-codes');
const logger = require('./shared/logger');
const passport = require('passport');
const routes = require('./router');

// eslint-disable-next-line no-unused-vars
const auth = require('./auth');

const app = express();

const corsOptions = {
  origin(origin, cb) {
    if (includes(config.security.allowedOrigins, origin) || !origin) {
      cb(null, true);
    } else {
      cb(null, true);
      // cb(new Error('Origin not allowed.'));
    }
  },
  credentials: true
};

app.use(cors(corsOptions));
app.use(cookieParser());
app.use(bodyParser.json({ limit: 1000000 }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(passport.initialize());

// Log all incoming requests
app.use('/api/v1', (req, res, next) => {
  logger.info({ req });
  next();
});

app.use('/favicon.ico', function (req, res) {
  res.status(NO_CONTENT);
});

// Enforce authentication on every route except the whitelisted ones
app.use('*', (req, res, next) => {
  const whitelisted = [
    '/api/v1/users/login',
    '/api/v1/users/register',
    '/api/v1/users/verify-account/*',
    '/api/v1/users/request-reset-password',
    '/api/v1/users/reset-password/*',
    '/api/v1/users/validate',
    '/api/v1/users/resend-verification',
    '/api/v1/users/token',
    '/api/v1/post/posts',
    '/api/v1/message/newsletter',
    '/api/v1/payment/stripe-hook'
  ];

  const fbUrls = [
    '/api/v1/users/fb/init',
    '/api/v1/users/fb/login'
  ];

  const isWhitelisted = (url, patterns) => {
    const whitelist = new RegExp(patterns.join('|'));
    return whitelist.test(url);
  };

  if (isWhitelisted(req.baseUrl, whitelisted)) {
    next();
  } else if (isWhitelisted(req.baseUrl, fbUrls)) {
    const fbOptions = {
      scope: ['public_profile', 'email'],
      failureRedirect: `${config.client.url}/signin`
    };
    passport.authenticate('fb', fbOptions)(req, res, next);
  } else {
    passport.authenticate('jwt')(req, res, next);
  }
});

// Hook main router
app.use('/api/v1', routes);

// Global error handler.
app.use((err, req, res, next) => {
  if (!err.status || err.status === INTERNAL_SERVER_ERROR) {
    res.status(INTERNAL_SERVER_ERROR).end();
    logger.error({ err });
    return;
  }
  const { status, message } = err;
  res.status(status).json({ error: { status, message } });
});

// Handle non-existing routes.
app.use((req, res, next) => res.status(NOT_FOUND).end());

module.exports = app;
