'use-strict';

const config = require('../../../config');
const Promise = require('bluebird');

async function createDefaultReferral(db) {
  const { code, email, firstName } = config.user;
  const query = { where: { email, firstName, verified: true } };
  const [user, created] = await db.User.findOrCreate(query);
  return user;
}

function initFixtures(db) {
  return Promise.join(
    createDefaultReferral(db)
  );
}

module.exports = initFixtures;
