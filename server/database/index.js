'use-strict';

const config = require('../../config');
const each = require('lodash/each');
const initFixtures = require('./fixtures');
const Sequelize = require('sequelize');
const values = require('lodash/values');

// Add sequelize hierarchy plugin
require('sequelize-hierarchy')(Sequelize);

const sequelize = new Sequelize(...values(config.db.auth), config.db.options);

let db = { Sequelize, sequelize };

// TODO: Better model loading
const models = {

  User: '../common/user/user.model',
  Message: '../common/message/message.model',
  Post: '../common/post/post.model',
  Video: '../common/video/video.model',
  Goals: '../common/goals/goals.model',
  Notification: '../common/notification/notification.model',
  Progression: '../common/progression/progression.model',
  Course: '../common/course/course.model',
  Played: '../common/played/played.model',
  Badge: '../common/badges/badges.model'
};

// Load modules
each(models, (path, name) => {
  db[name] = sequelize.import(path);
});

// Add associations (relations)
each(db, (v, modelName) => {
  if ('associate' in db[modelName]) db[modelName].associate(db);
});

db.initialize = () => sequelize.sync().then(() => initFixtures(db));

module.exports = db;
