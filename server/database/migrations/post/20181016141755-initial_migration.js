'use strict';

const values = require('lodash/values');
const config = require('../../../../config');

module.exports = {
  up: (queryInterface, Sequelize) => {
    const { DATE, STRING, UUID, UUIDV4, BOOLEAN , TEXT} = Sequelize;

    return queryInterface.createTable('post', {
      id: {
        type: UUID,
        primaryKey: true,
        defaultValue: UUIDV4
      },
      title: {
        type: STRING,
        allowNull: false,
        field: 'title'
      },
      postBody: {
        type: TEXT,
        allowNull: false,
        field: 'post_body'
      },
      author: {
        type: UUID,
        allowNull: false,
        field: 'author'
      },
      isNewsletter: {
        type: BOOLEAN,
        validate: { notEmpty: true },
        allowNull: false,
        defaultValue: false,
        field: 'is_newsletter'
      },
      createdAt: {
        type: DATE,
        field: 'created_at'
      },
      updatedAt: {
        type: DATE,
        field: 'updated_at'
      }
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('post');
  }
};
