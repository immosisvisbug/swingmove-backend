'use strict';

const values = require('lodash/values');
const config = require('../../../../config');

module.exports = {
  up: (queryInterface, Sequelize) => {
    const { DATE, STRING, UUID, UUIDV4, INTEGER } = Sequelize;

    return queryInterface.createTable('goals', {
      userId: {
        type: UUID,
        allowNull: false,
        unique: true,
        defaultValue: UUIDV4,
        primaryKey: true,
        field: 'user_id',
        onUpdate: 'CASCADE',
        onDelete: 'RESTRICT',
        references: {
          model: 'user',
          key: 'id'
        }
      },
      streak: {
        type: INTEGER,
        allowNull: false,
        defaultValue: false,
        field: 'streak'
      },
      dayStreak: {
        type: INTEGER,
        allowNull: false,
        defaultValue: 0,
        field: 'day_streak'
      },
      latestWorkoutWatched: {
        type: UUID,
        allowNull: true,
        field: 'latest_workout'
      },
      lastWorkoutGoal: {
        type: DATE,
        allowNull: true,
        field: 'latest_goal'
      },
      dailyWorkoutGoal: {
        type: INTEGER,
        allowNull: false,
        defaultValue: false,
        field: 'workout_goal'
      },
      createdAt: {
        type: DATE,
        field: 'created_at'
      },
      updatedAt: {
        type: DATE,
        field: 'updated_at'
      }
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('goals');
  }
};
