module.exports = {

  up: function (queryInterface, Sequelize) {
    // logic for transforming into the new state
    const { STRING} = Sequelize;
    return queryInterface.addColumn(
      'video',
      'category',
      STRING
    );
  }

};
