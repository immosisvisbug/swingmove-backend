'use strict';

const values = require('lodash/values');
const config = require('../../../../config');

module.exports = {
  up: (queryInterface, Sequelize) => {
    const { DATE, STRING, UUID, UUIDV4, DOUBLE} = Sequelize;

    return queryInterface.createTable('video', {
      id: {
        type: UUID,
        primaryKey: true,
        defaultValue: UUIDV4
      },
      videoUrl: {
        type: STRING,
        field: 'video_url'
      },
      title: {
        type: STRING,
        field: 'title'
      },
      courseId: {
        type: UUID,
        allowNull: false,
        unique: true,
        defaultValue: UUIDV4,
        primaryKey: true,
        field: 'course_id',
        onUpdate: 'CASCADE',
        onDelete: 'RESTRICT',
        references: {
          model: 'Course',
          key: 'id'
        }
      },
      videoLength: {
        type: DOUBLE,
        field: 'video_length'
      },
      category: {
        type: STRING,
        field: 'category'
      },
      createdAt: {
        type: DATE,
        field: 'created_at'
      },
      updatedAt: {
        type: DATE,
        field: 'updated_at'
      }
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('video');
  }
};
