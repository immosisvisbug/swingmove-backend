'use strict';

const values = require('lodash/values');
const config = require('../../../../config');

module.exports = {
  up: (queryInterface, Sequelize) => {
    const { DATE, STRING, UUID, UUIDV4 } = Sequelize;

    return queryInterface.createTable('played', {
      userId: {
        type: UUID,
        primaryKey: true,
        field: 'user_id',
        onUpdate: 'CASCADE',
        onDelete: 'RESTRICT',
        references: {
          model: 'User',
          key: 'id'
        }
      },
      videoWatched: {
        type: UUID,
        primaryKey: true,
        field: 'video_watched',
        onUpdate: 'CASCADE',
        onDelete: 'RESTRICT',
        references: {
          model: 'Video',
          key: 'id'
        }
      },
      courseId: {
        type: UUID,
        allowNull: false,
        unique: true,
        defaultValue: UUIDV4,
        primaryKey: true,
        field: 'course_id',
        onUpdate: 'CASCADE',
        onDelete: 'RESTRICT',
        references: {
          model: 'Course',
          key: 'id'
        }
      },
      progress: {
        type: STRING,
        allowNull: true,
        field: 'time_watched'
      },
      lastPlayed: {
        type: DATE,
        allowNull: false,
        field: 'last_played'
      },

      createdAt: {
        type: DATE,
        field: 'created_at'
      },
      updatedAt: {
        type: DATE,
        field: 'updated_at'
      }
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('played');
  }
};
