'use strict';

const values = require('lodash/values');
const config = require('../../../../config');

module.exports = {
  up: (queryInterface, Sequelize) => {
    const { BOOLEAN, DATE, DATEONLY, ENUM, STRING, UUID, UUIDV4 } = Sequelize;

    return queryInterface.createTable('user', {
      id: {
        type: UUID,
        primaryKey: true,
        defaultValue: UUIDV4
      },
      facebookId: {
        type: STRING,
        unique: true,
        field: 'facebook_id'
      },
      stripeCustomerId: {
        type: STRING,
        unique: true,
        field: 'stripe_customer_id'
      },
      firstName: {
        type: STRING,
        validate: { notEmpty: true, len: [0, 50] },
        allowNull: false,
        field: 'first_name'
      },
      middleName: {
        type: STRING,
        validate: { len: [0, 50] },
        field: 'middle_name'
      },
      lastName: {
        type: STRING,
        validate: { len: [0, 50] },
        field: 'last_name'
      },
      email: {
        type: STRING,
        validate: { notEmpty: true, isEmail: true },
        allowNull: false,
        unique: true
      },
      password: {
        type: STRING,
        validate: { notEmpty: true, len: [3, 100] }
      },
      verified: {
        type: BOOLEAN,
        validate: { notEmpty: true },
        allowNull: false,
        defaultValue: false
      },
      profilePicture: {
        type: STRING,
        validate: { isUrl: true, notEmpty: true },
        allowNull: true,
        field: 'profile_picture'
      },
      membership: {
        type: ENUM(values(config.membership)),
        defaultValue: config.membership.FREE
      },
      plan: {
        type: ENUM(values(config.plans)),
        defaultValue: config.plans.FREE
      },
      timeZone: {
        type: STRING,
        allowNull: false
      },
      createdAt: {
        type: DATE,
        field: 'created_at'
      },
      updatedAt: {
        type: DATE,
        field: 'updated_at'
      }
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('user');
  }
};
