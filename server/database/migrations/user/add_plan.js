'use strict';

const values = require('lodash/values');
const config = require('../../../../config');

module.exports = {
  up: function (queryInterface, Sequelize) {
    const { ENUM } = Sequelize;

    // logic for transforming into the new state
    return queryInterface.addColumn(
      'user',
      'plan',
      ENUM(values(config.plans))
    );
  }

};
