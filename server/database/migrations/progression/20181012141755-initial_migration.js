'use strict';

const values = require('lodash/values');
const config = require('../../../../config');

module.exports = {
  up: (queryInterface, Sequelize) => {
    const { DATE, STRING, UUID, UUIDV4 } = Sequelize;

    return queryInterface.createTable('progression', {
      userId: {
        type: UUID,
        primaryKey: true,
        field: 'user_id',
        onUpdate: 'CASCADE',
        onDelete: 'RESTRICT',
        references: {
          model: 'User',
          key: 'id'
        }
      },
      videoWatched: {
        type: UUID,
        primaryKey: true,
        field: 'video_watched',
        onUpdate: 'CASCADE',
        onDelete: 'RESTRICT',
        references: {
          model: 'Video',
          key: 'id'
        }

      },

    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('progression');
  }
};
