'use strict';

const values = require('lodash/values');
const config = require('../../../../config');

module.exports = {
  up: (queryInterface, Sequelize) => {
    const { DATE, STRING, UUID, UUIDV4 } = Sequelize;

    return queryInterface.createTable('message', {
      id: {
        type: UUID,
        primaryKey: true,
        defaultValue: UUIDV4
      },
      title: {
        type: STRING,
        field: 'title'
      },
      description: {
        type: STRING,
        field: 'description'
      },
      userId: {
        type: UUID,
        allowNull: false,
        field: 'user_id'
      },
      createdAt: {
        type: DATE,
        field: 'created_at'
      },
      updatedAt: {
        type: DATE,
        field: 'updated_at'
      }
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('message');
  }
};
