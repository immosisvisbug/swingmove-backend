'use strict';

const values = require('lodash/values');
const config = require('../../../../config');

module.exports = {
  up: (queryInterface, Sequelize) => {
    const { DATE, STRING, UUID, UUIDV4, INTEGER } = Sequelize;

    return queryInterface.createTable('badge', {
      userId: {
        type: UUID,
        allowNull: false,
        unique: true,
        defaultValue: UUIDV4,
        primaryKey: true,
        field: 'user_id',
        onUpdate: 'CASCADE',
        onDelete: 'RESTRICT',
        references: {
          model: 'user',
          key: 'id'
        }
      },
      highestStreak: {
        type: INTEGER,
        allowNull: false,
        defaultValue: 0,
        field: 'streak'
      }
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('badge');
  }
};
