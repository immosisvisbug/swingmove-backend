'use strict';

const values = require('lodash/values');
const config = require('../../../../config');

module.exports = {
  up: (queryInterface, Sequelize) => {
    const { DATE, STRING, UUID, UUIDV4, INTEGER } = Sequelize;

    return queryInterface.createTable('course', {
      id: {
        type: UUID,
        primaryKey: true,
        defaultValue: UUIDV4
      },
      title: {
        type: STRING,
        field: 'title'
      },
      latestWorkoutWatched: {
        type: UUID,
        allowNull: true,
        field: 'video_watched',
        onUpdate: 'CASCADE',
        onDelete: 'RESTRICT',
        references: {
          model: 'Video',
          key: 'id'
        }
      },
      latestWorkoutWatchedProgress: {
        type: STRING,
        allowNull: true,
        field: 'time_watched'
      },
      createdAt: {
        type: DATE,
        field: 'created_at'
      },
      updatedAt: {
        type: DATE,
        field: 'updated_at'
      }
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('course');
  }
};
