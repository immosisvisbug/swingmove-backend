module.exports = {

  up: function (queryInterface, Sequelize) {
    // logic for transforming into the new state
    const { DATE} = Sequelize;
    return [
      queryInterface.addColumn(
        'course',
        'created_at',
        DATE
      ),
      queryInterface.addColumn(
        'course',
        'updated_at',
        DATE
      )
    ];
  }

};
