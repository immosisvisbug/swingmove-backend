'use strict';

const values = require('lodash/values');
const config = require('../../../../config');

module.exports = {
  up: (queryInterface, Sequelize) => {
    const { DATE, STRING, UUID, UUIDV4, ENUM, INTEGER } = Sequelize;

    return queryInterface.createTable('notification', {
      id: {
        type: UUID,
        primaryKey: true,
        defaultValue: UUIDV4
      },
      userId: {
        type: UUID,
        allowNull: false,
        field: 'user_id',
        onUpdate: 'CASCADE',
        onDelete: 'RESTRICT',
        references: {
          model: 'user',
          key: 'id'
        }
      },
      type: {
        type: ENUM(values(config.notificationType)),
        allowNull: false,
        field: 'type'
      },
      startTime: {
        type: DATE,
        allowNull: true,
        field: 'start_time'
      },
      endTime: {
        type: DATE,
        allowNull: true,
        field: 'end_time'
      },
      enabled: {
        field: 'enabled',
        allowNull: false,
        type: BOOLEAN,
        defaultValue: false
      },
      interval: {
        type: INTEGER,
        allowNull: true
      },
      dailyTime: {
        type: DATE,
        allowNull: true,
        field: 'daily_time'
      },
      createdAt: {
        type: DATE,
        field: 'created_at'
      },
      updatedAt: {
        type: DATE,
        field: 'updated_at'
      }
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('notification');
  }
};
