'use-strict';

const {
  getVideo
} = require('../common/video/video.storage');

async function addUrlToVideo(videos) {
  for (video in videos) {
    let current_video = videos[video];
    current_video.videoUrl = await getVideo(current_video.videoUrl);
  }
  return videos
}

module.exports = { addUrlToVideo };
