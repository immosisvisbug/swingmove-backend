'use-strict';

const config = require('../../config');
const nodemailer = require('nodemailer');
const Promise = require('bluebird');
const stubTransport = require('nodemailer-stub-transport');
const mandrillTransport = require('nodemailer-mandrill-transport');
const Mailchimp = require('mailchimp-api-v3');
const md5 = require('md5');

/*
const mailer = config.env !== 'production'
  ? nodemailer.createTransport(stubTransport())
  : nodemailer.createTransport({
    auth: {
      user: config.mail.user,
      pass: config.mail.pass
    },
    host: config.mail.host,
    port: config.mail.port,
    secure: true
  });
*/

var mailer = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'swingmoveapp@gmail.com',
    pass: 'Hello1234!'
  }
});

// var smtpTransport = nodemailer.createTransport("smtps://" + encodeURIComponent('devesh@visbug.com') + ":" + encodeURIComponent('st.maarten') + "@smtp.gmail.com:465");

const logMail = mail => {
  console.log('\n\n==================== ENVELOPE ==================== ');
  console.log(JSON.stringify(mail.envelope, null, 4));

  console.log('\n\n====================== MAIL ====================== ');
  console.log(mail.response.toString());
};

function send(message, transporter = mailer) {
  return new Promise((resolve, reject) => {
    transporter.sendMail(message, (err, mail) => {
      if (err) reject(err);
      // if (config.env !== 'development') logMail(mail);
      resolve(mail);
    });
  });
}

const mandrillMailer = nodemailer.createTransport(
  mandrillTransport({
    auth: {
      apiKey: config.email.mandrill
    }
  })
);

function sendMandrill(email, transporter = mandrillMailer) {
  const variables = email.variables.map(v => {
    for (let key in v) {
      return {
        name: key,
        content: v[key]
      };
    }
  });
  const sendConfig = {
    from: `SwingMove <${config.mail.address}>`,
    to: email.to,
    subject: email.subject,
    mandrillOptions: {
      async: true,
      template_name: email.template,
      template_content: [],
      message: {
        global_merge_vars: variables
      },
      send_at: email.send_at != undefined ? email.send_at : new Date()
    }
  };

  return new Promise((resolve, reject) => {
    transporter.sendMail(sendConfig, (err, mail) => {
      if (err) return reject(new Error(err.message));
      if (mail.rejected.length > 0) {
        return reject(new Error(mail.rejected[0].reject_reason));
      }
      resolve(mail);
    });
  });
}

const mailchimpMailer = new Mailchimp(config.email.mailchimp);

function subscribeMailChimp(user, list, variables, transporter = mailchimpMailer) {
  const url = `/lists/${config.email.newsletters[list]}/members`;
  const defaultMF = {
    FNAME: user.firstName,
    LNAME: user.lastName ? user.lastName : ' '
  };
  const mergeFields = Object.assign({}, defaultMF, variables);
  return new Promise((resolve, reject) => {
    transporter.post(url, {
      email_address: user.email,
      status: 'subscribed',
      merge_fields: mergeFields
    }).then(result => resolve(result))
      .catch(error => reject(error));
  });
}

function unSubscribeMailChimp(email, list, transporter = mailchimpMailer) {
  const u = md5(email.toLowerCase());
  const url = `/lists/${config.email.newsletters[list]}/members/${u}`;

  return new Promise((resolve, reject) => {
    transporter.patch(url, {
      status: 'unsubscribed'
    }).then(result => resolve(result))
      .catch(error => reject(error));
  });
}
module.exports = {
  send,
  sendMandrill,
  subscribeMailChimp,
  unSubscribeMailChimp
};
