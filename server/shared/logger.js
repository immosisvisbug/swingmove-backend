'use-strict';

const bunyan = require('bunyan');
const config = require('../../config');
const pick = require('lodash/pick');

const logger = bunyan.createLogger({
  name: 'wmn-api-logger',
  serializers: {
    ...bunyan.stdSerializers,
    migration: migrationSerializer
  }
});

function migrationSerializer(migration) {
  return pick(migration, ['file', 'path', 'stack']);
}

// Mute logger output during test runs
if (config.env === 'test') {
  logger.level(bunyan.FATAL + 1);
}

module.exports = logger;
