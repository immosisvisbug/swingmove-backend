'use-strict';

const config = require('../../config');
const get = require('lodash/get');
const Promise = require('bluebird');
const stripe = require('stripe')(config.stripe.secret);
const { User } = require('../database');

async function createCustomer(user) {
  return stripe.customers.create({
    email: user.email,
    metadata: { userId: user.id }
  });
};

async function getNextBillingDate(user) {
  if (!user.stripeCustomerId) {
    return null;
  }

  let customerId = user.stripeCustomerId;
  const customer = await stripe.customers.retrieve(customerId);
  const subscription = get(customer.subscriptions, 'data[0]');

  if (!subscription) {
    return null
  }

  let nextCycle = subscription.current_period_end ? subscription.current_period_end : subscription.trial_end;
  var date = new Date(1970, 0, 1); // Epoch
  date.setSeconds(nextCycle);

  return date;
}

async function cancelSubscription(customerId) {
  const customer = await stripe.customers.retrieve(customerId);
  const subscription = get(customer.subscriptions, 'data[0]');

  return stripe.subscriptions.del(subscription.id);
}

async function createSubscription(customerId, type, source, coupon, trialDays) {
  let result = await stripe.customers.createSource(customerId, {
    source: source
  });

  let options = {
    customer: customerId,
    items: [
      { plan: config.stripe.plan[type] }
    ]
  };
  if (coupon) options.coupon = coupon;

  if (trialDays) options.trial_period_days = trialDays;

  return stripe.subscriptions.create(options);
}

async function changeSubscription(customerId, type, source = null, coupon = null, trialDays = null) {
  return config.membership[type] === config.membership.FREE
    ? cancelSubscription(customerId)
    : createSubscription(customerId, type, source, coupon, trialDays);
}

async function fetchCoupon(coupon) {
  return stripe.coupons.retrieve(coupon);
}

async function fetchPlan(plan) {
  return stripe.plans.retrieve(plan);
}

async function getPlanDiscount(plan, coupon) {
  [coupon, plan] = await Promise.join(fetchCoupon(coupon), fetchPlan(plan));
  let { amount: price } = plan;
  let { amount_off: amountOff, percent_off: percentOff } = coupon;
  let discount = amountOff || (percentOff * price) / 100;
  return Math.max(0, (price - discount) / 100);
}

async function getMembershipPrice(membership, coupon) {
  let plan = config.stripe.plan[membership];
  return getPlanDiscount(plan, coupon);
}

module.exports = {
  createCustomer,
  changeSubscription,
  fetchCoupon,
  fetchPlan,
  getPlanDiscount,
  getMembershipPrice,
  getNextBillingDate
};
