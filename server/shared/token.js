'use-strict';

const config = require('../../config');
const jwt = require('jsonwebtoken');

function createToken(payload = {}, options = {}) {
  return jwt.sign(payload, config.auth.jwtSecret, options);
}

function verifyToken(token, options = {}) {
  return jwt.verify(token, config.auth.jwtSecret, options)
}
module.exports = { createToken, verifyToken };
