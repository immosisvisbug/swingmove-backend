'use-strict';

const { BAD_REQUEST } = require('http-status-codes');
const httpError = require('http-errors');

function createHttpError(code = BAD_REQUEST, message = 'An error has occured') {
  return Promise.reject(httpError(code, message));
}

module.exports = { createHttpError };
