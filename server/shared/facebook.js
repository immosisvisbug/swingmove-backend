'use-strict';

const crypto = require('crypto');
const path = require('path');
const request = require('request-promise');

async function fetchProfilePicture(url) {
  let rawImage = await request.get({ url, encoding: null });
  let file = Buffer.from(rawImage, 'utf8');
  let nameFormat = `${url} - ${Date.now()}`;
  let name = crypto.createHash('md5').update(nameFormat).digest('hex');
  name = path.format({ name, ext: '.jpg' });
  return { name, file, type: 'image/jpeg' };
}

module.exports = { fetchProfilePicture };
