'use-strict';

const app = require('./app');
const config = require('../config');
const db = require('./database');
const Promise = require('bluebird');

function runApp() {
  return new Promise((resolve, reject) => {
    app.listen(config.server.port, error => error ? reject(error) : resolve());
  });
}

async function start() {
  try {
    await db.initialize();
    // console.log('Database initialized!');
    await runApp();
    console.log(`Server running on: ${config.server.url}`);
  } catch (err) {
    console.error(`Error: ${err}`);
  }
}

start();
