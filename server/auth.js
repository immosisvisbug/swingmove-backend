'use-strict';

const config = require('../config');
const { Strategy: JWTStrategy } = require('passport-jwt');
const get = require('lodash/get');
const passport = require('passport');
const { Strategy: FbStrategy } = require('passport-facebook');
const { User } = require('./database');
const passportJWT = require('passport-jwt');
const ExtractJWT = passportJWT.ExtractJwt;


const jwtOptions = {
  secretOrKey: config.auth.jwtSecret,
  jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken()
};

const fbFields = [
  'id',
  'first_name',
  'last_name',
  'email',
  'picture.width(200).height(200)'
];

const fbOptions = {
  clientID: config.facebook.id,
  clientSecret: config.facebook.secret,
  callbackURL: `${config.server.url}/api/v1/users/fb/login`,
  profileFields: fbFields,
  profileURL: 'https://graph.facebook.com/v2.9/me'
};

function extractJwtFromCookie(req) {
  return get(req, 'cookies.jwt');
}

async function verifyJwt(payload, done) {
  try {
    const user = await User.findById(payload.id);
    done(null, user || false);
  } catch (err) {
    done(err, false);
  }
}

async function verifyFb(accessToken, refreshToken, profile, done) {
  const {
    id: facebookId,
    first_name: firstName,
    last_name: lastName,
    email,
    picture
  } = profile._json;

  const data = {
    facebookId,
    firstName,
    lastName,
    email,
    pictureUrl: get(picture, 'data.url')
  };

  try {
    const user = await User.findOrCreateFbUser(data);
    done(null, user || false);
  } catch (err) {
    done(err, false);
  }
}

passport.use('jwt', new JWTStrategy(jwtOptions, verifyJwt));
passport.use('fb', new FbStrategy(fbOptions, verifyFb));

passport.serializeUser((user, done) => {
  done(null, user);
});

passport.deserializeUser((user, done) => {
  done(null, user);
});
