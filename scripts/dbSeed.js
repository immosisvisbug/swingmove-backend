'use-strict';

const db = require('../server/database');
const Promise = require('bluebird');
const seedUsers = require('../server/common/user/seed');

async function seed() {
  try {
    await db.initialize();
    await Promise.join(seedUsers(db));
    console.log('Seed successfull.');
    process.exit(0);
  } catch (err) {
    console.error(err);
    process.exit(1);
  }
}

seed();
