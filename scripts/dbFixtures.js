'use-strict';

const db = require('../server/database');
const initFixtures = require('../server/database/fixtures');

initFixtures(db)
  .then(() => {
    console.log('Fixtures initialized successfully!');
    process.exit(0);
  })
  .catch(err => {
    console.error(err);
    process.exit(1);
  });
