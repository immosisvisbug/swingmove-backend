'use-strict';

const { exec } = require('child_process');
const path = require('path');

/**
 * Programatically execute npm command.
 * @param {string} cmd - Command to execute
 */
function execCmd(cmd) {
  return exec(cmd, (err, stdout, stderr) => {
    if (!err) {
      process.stdout.write(stdout);
      process.stderr.write(stderr);
      process.exit(0);
    }
    console.error(err.message);
    console.error(err.stack);
    process.exit(0);
  });
}

/**
 * Create an absolute path starting from project root.
 * @param {array} args - Path arguments collected in an array.
 * @returns {string} Absolute path starting from project root up to args.
 */
function projectRoot(...args) {
  const root = path.resolve(__dirname, '../');
  return path.join(root, ...args);
}

module.exports = {
  execCmd,
  projectRoot
};
