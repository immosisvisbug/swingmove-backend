'use-strict';

const db = require('../server/database');

db.sequelize.drop()
  .then(() => {
    return db.sequelize.query('DROP TABLE IF EXISTS "migrations";');
  })
  .then(() => {
    console.log('Database has been reset!');
    process.exit(0);
  })
  .catch(err => {
    console.error(err.message);
    process.exit(1);
  });
