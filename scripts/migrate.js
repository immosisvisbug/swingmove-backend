'use-strict';

const { execCmd, projectRoot } = require('./helpers');
const get = require('lodash/get');
const logger = require('../server/shared/logger');
const path = require('path');
const Promise = require('bluebird');
const { sequelize } = require('../server/database');
const Umzug = require('umzug');
const yargs = require('yargs');

const MIGRATIONS_FOLDER = projectRoot('server', 'database', 'migrations');

/**
 * Create new global or module specific Umzug instance.
 * @param {string} module - Module name for module specific migrations.
 * @returns {Umzug}
 */
function spawnUmzug(module = null) {
  return new Umzug({
    storage: 'sequelize',
    storageOptions: {
      sequelize,
      tableName: 'migrations'
    },
    migrations: {
      params: [
        sequelize.getQueryInterface(),
        sequelize.constructor
      ],
      traverseDirectories: !module,
      path: projectRoot('server', 'database', 'migrations', module || ''),
      pattern: /\.js$/
    }
  });
}

/**
 * Format error object for migration logger serializer.
 * @param {object} err - Error object inside catch block.
 * @returns {array} Destructured inside logger.catch cb.
 */
function errorFormat(file, err) {
  return [{ migration: { file, stack: err.stack } }, err.message];
}

/**
 * Create new module specific migration file.
 * @param {string} module - Module name.
 * @param {string} name - Migration name.
 */
function createMigration({ module, name }) {
  let cmd = [
    'sequelize migration:create',
    `--migrations-path=${path.join(MIGRATIONS_FOLDER, module)}`,
    `--name=${name}`
  ];
  execCmd(cmd.join(' '));
}

/**
 * Revert last overall migration, last migration for module, or specific
 * migration in module.
 * @param {string} module - Module name.
 * @param {string} name - Migration name.
 */
async function revertMigrations({ module = null, name = null }) {
  const umzug = spawnUmzug(module);
  const query = { order: [['name', 'DESC']] };
  const { SequelizeMeta } = sequelize.models;

  let file = name || get(await SequelizeMeta.findOne(query), 'name', '');
  try {
    let [migration] = await umzug.down(file.name);
    logger.info({ migration }, 'migration successfully reverted');
    process.exit(0);
  } catch (err) {
    logger.error(...errorFormat(file.name, err));
    process.exit(1);
  }
}

/**
 * Apply all migrations, all migrations in module or specific migration
 * in module.
 * @param {string} module - Module name.
 * @param {string} name - Migration name.
 */
async function applyMigrations({ module = null, name = null }) {
  const umzug = spawnUmzug(module);

  let files = name ? [{ file: name }] : await umzug.pending();
  await Promise.each(files, async ({ file }) => {
    try {
      let [migration] = await umzug.up(file);
      logger.info({ migration }, 'migration successfully applied');
    } catch (err) {
      logger.error(...errorFormat(file, err));
      process.exit(1);
    }
  });

  process.exit(0);
}

/* eslint-disable */
yargs
  .command({
    command: 'create <module> <name>',
    desc: 'Create migration in <module> folder with <name> filename.',
    handler: createMigration
  })
  .command({
    command: 'apply [module] [name]',
    desc: 'Apply all migrations, all migrations for [module] or a single migration for [name].',
    handler: applyMigrations
  })
  .command({
    command: 'revert <module> [name]',
    desc: 'Revert last migration in <module> or specific migration with [name].',
    handler: revertMigrations
  })
  .showHelpOnFail(true)
  .demand(1, '')
  .help('help')
  .argv;
