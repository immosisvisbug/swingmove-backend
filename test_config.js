'use-strict';

// Load env vars
const dotenv = require('dotenv');
const dotenvExpand = require('dotenv-expand');
dotenvExpand(dotenv.config({ path: './.env' }));

const db = require('./server/database');

before(function () {
  return db.sequelize.sync({ force: true });
});
