'use-strict';

const config = require('./base');
const merge = require('lodash/merge');

if (process.env.NODE_ENV !== 'production') {
  let localConfig;
  try {
    localConfig = require(`./${config.env}`) || {};
  } catch (err) {
    localConfig = {};
  }

  merge(config, localConfig);
}

module.exports = config;
