'use-strict';

module.exports = {
  env: process.env.NODE_ENV || 'development',
  auth: {
    saltRounds: parseInt(process.env.AUTH_SALT_ROUNDS || 10, 10),
    jwtSecret: process.env.AUTH_JWT_SECRET
  },
  client: {
    domain: process.env.CLIENT_DOMAIN,
    port: parseInt(process.env.CLIENT_PORT || 4200, 10),
    url: process.env.CLIENT_URL
  },
  db: {
    auth: {
      name: process.env.DB_NAME,
      user: process.env.DB_USER,
      pass: process.env.DB_PASS
    },
    options: {
      host: process.env.DB_HOST || '127.0.0.1',
      port: process.env.DB_PORT || 5432,
      dialect: process.env.DB_DIALECT || 'postgres',
      logging: process.env.NODE_ENV !== 'test'
    }
  },
  facebook: {
    id: process.env.FB_APP_ID,
    secret: process.env.FB_APP_SECRET,
    clientRedirectUrl: process.env.FB_CLIENT_REDIRECT
  },
  stripe: {
    secret: process.env.STRIPE_SECRET_KEY,
    webhook: process.env.STRIPE_WEBHOOK_KEY,
    currency: process.env.STRIPE_CURRENCY,
    plan: {
      MONTHLY: process.env.STRIPE_MONTHLY_PLAN,
      YEARLY: process.env.STRIPE_YEARLY_PLAN
    }
  },
  plans: {
    MONTHLY: 'MONTHLY',
    YEARLY: 'YEARLY',
    FREE: 'FREE'
  },
  membership: {
    FREE: 'FREE',
    PAID: 'PAID',
    ADMIN: 'ADMIN'
  },
  notificationType: {
    DAILY: 'DAILY',
    INTERVAL: 'INTERVAL'
  },
  mail: {
    user: process.env.EMAIL_USER,
    pass: process.env.EMAIL_PASS,
    host: process.env.EMAIL_HOST,
    port: process.env.EMAIL_PORT,
    address: process.env.EMAIL_ADDRESS
  },
  security: {
    allowedOrigins: (process.env.CORS_ALLOWED_ORIGINS || 'https://desolate-plains-71884.herokuapp.com:3000,https://desolate-plains-71884.herokuapp.com, http://localhost:4200, https://swingmove.com')
      .split(',')
      .map(origin => origin.trim())
  },
  server: {
    port: parseInt(process.env.PORT || 3000, 10),
    url: process.env.SERVER_URL
  },
  storage: {
    provider: 'amazon',
    amazon: {
      key: process.env.AWS_S3_ID,
      secret: process.env.AWS_S3_SECRET,
      region: process.env.AWS_S3_REGION,
      bucket: process.env.AWS_S3_BUCKET
    }
  },
  user: {
    code: process.env.DEFAULT_USER_CODE,
    email: process.env.DEFAULT_USER_EMAIL,
    firstName: process.env.DEFAULT_USER_FIRSTNAME
  },
  email: {
    mandrill: process.env.MANDRILL_KEY,
    mailchimp: process.env.MAILCHIMP_KEY,
    newsletters: {
      defaultList: 'fa7b99ea1d'
    }
  },
  badges: {
    ONE: 1,
    SEVEN: 7,
    FOURTEEN: 14,
    THIRTY: 30
  }
};
