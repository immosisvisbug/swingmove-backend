'use-strict';

const config = require('./base');

module.exports = {
  db: {
    auth: {
      name: `${config.db.auth.name}_test`
    }
  }
};
